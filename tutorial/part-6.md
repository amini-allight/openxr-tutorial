This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 6: The OpenXR swapchain

Now that we have an OpenXR session we can use that to create an OpenXR swapchain. This part is probably going to be quite familiar to anyone who's used Vulkan on a regular flat display before. The graphics device will essentially have a "chain" of images, at least two but often more, and it can display any of them to the physical screen. At any instant in time one of the images will be displayed on the physical screen while at least one other is being drawn to by the application. The active image is periodically "swapped" to one drawn more recently at the display's refresh rate. Because this is OpenXR we're going to be creating two swapchains, one for each eye, and these will be created via OpenXR, unlike `VkSwapchainKHR` for flat displays which is created almost entirely within Vulkan.

First we're going to define a constant for the number of eyes a human typically has because this is going to come up over and over.

```
static const size_t eyeCount = 2;
```

Then we're going to create a pair of swapchains:

```
tuple<Swapchain*, Swapchain*> createSwapchains(XrInstance instance, XrSystemId system, XrSession session)
{
    uint32_t configViewsCount = eyeCount;
    vector<XrViewConfigurationView> configViews(
        configViewsCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    XrResult result = xrEnumerateViewConfigurationViews(
        instance,
        system,
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewsCount,
        &configViewsCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate view configuration views: " << result << endl;
        return { nullptr, nullptr };
    }

    uint32_t formatCount = 0;

    result = xrEnumerateSwapchainFormats(session, 0, &formatCount, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain formats: " << result << endl;
        return { nullptr, nullptr };
    }

    vector<int64_t> formats(formatCount);

    result = xrEnumerateSwapchainFormats(session, formatCount, &formatCount, formats.data());

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain formats: " << result << endl;
        return { nullptr, nullptr };
    }

    int64_t chosenFormat = formats.front();

    for (int64_t format : formats)
    {
        if (format == VK_FORMAT_R8G8B8A8_SRGB)
        {
            chosenFormat = format;
            break;
        }
    }

    XrSwapchain swapchains[eyeCount];

    for (uint32_t i = 0; i < eyeCount; i++)
    {
        XrSwapchainCreateInfo swapchainCreateInfo{};
        swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
        swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
        swapchainCreateInfo.format = chosenFormat;
        swapchainCreateInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
        swapchainCreateInfo.width = configViews[i].recommendedImageRectWidth;
        swapchainCreateInfo.height = configViews[i].recommendedImageRectHeight;
        swapchainCreateInfo.faceCount = 1;
        swapchainCreateInfo.arraySize = 1;
        swapchainCreateInfo.mipCount = 1;

        result = xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]);

        if (result != XR_SUCCESS)
        {
            cerr << "Failed to create swapchain: " << result << endl;
            return { nullptr, nullptr };
        }
    }

    return {
        new Swapchain(
            swapchains[0],
            (VkFormat)chosenFormat,
            configViews[0].recommendedImageRectWidth,
            configViews[0].recommendedImageRectHeight
        ),
        new Swapchain(
            swapchains[1],
            (VkFormat)chosenFormat,
            configViews[1].recommendedImageRectWidth,
            configViews[1].recommendedImageRectHeight
        )
    };
}
```

We're going to be using this pattern with a pair of calls to functions called `xrEnumerate*` a few times. The arguments to these functions are laid out like so: `(..., countInput, countOutput, dataOutput)`. The first call leaves `dataOutput` as `nullptr`, this causes OpenXR to write the number of items available into the number pointed to by `countOutput`. We then use that number to allocate an array of the appropriate length (using `std::vector` to simply the memory management of said array). Then we feed that number back in as `countInput` in the second call, which writes the desired objects into our array.

First we get a pair of `XrViewConfigurationView` objects. These contain the recommended and maximum widths and heights for the images in the swapchain, as well as a recommended and maximum number of samples for multi-sample anti-aliasing (MSAA). Then we get a list of supported Vulkan image formats (which are returned as `int64_t` to avoid use of Vulkan specific types in a non-Vulkan specific part of the API). We iterate through this list to find one that suits our needs, in this case just searching for one of the most common image formats. This needs to match the format we used to create our `VkRenderPass` when we were setting up Vulkan. In a real application you may want to change this to be more flexible and support a wider variety of output formats (i.e. HDR output formats for devices with hardware HDR support) and initialize the render passes _after_ creating the swapchains to prevent incompatibility between them.

Finally we actually create the swapchains. We set usage flags for the Vulkan images which comprise the swapchain, which in this case is simply `XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT` because we're going to be drawing directly to these images. If you are going to be using these images in other ways i.e. transferring data to them with a simple copy operation, then you may wish to use different flags here such as `XR_SWAPCHAIN_USAGE_TRANSFER_DST_BIT`. We'll set the format to the one we picked earlier and take the session's advice on what width and height to use. Normally we would take the session's advice on what level of MSAA to use as well, but using MSAA complicates the initialization of several Vulkan objects (render passes and graphics pipelines cannot be created until the level of MSAA is known) so we'll just use 1 sample to simplify the tutorial, which means no MSAA.

Finally we set a bunch of numbers that are basically always 1 and create the swapchain.

Before we can return the swapchains its worth noting that for the following steps we're going to need the format, width and height of these swapchains, so we'll create a new structure to store that information alongside our swapchain object:

```
struct Swapchain
{
    Swapchain(XrSwapchain swapchain, VkFormat format, uint32_t width, uint32_t height)
        : swapchain(swapchain)
        , format(format)
        , width(width)
        , height(height)
    {

    }

    ~Swapchain()
    {
        xrDestroySwapchain(swapchain);
    }

    XrSwapchain swapchain;
    VkFormat format;
    uint32_t width;
    uint32_t height;
};
```

Instead of a function this time we'll just be using the `delete` operator on these two structures in the main function.

We're also going to need to get the actual images that comprise the swapchain so that we can draw things onto them with Vulkan:

```
vector<XrSwapchainImageVulkanKHR> getSwapchainImages(XrSwapchain swapchain)
{
    uint32_t imageCount;

    XrResult result = xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain images: " << result << endl;
        return {};
    }

    vector<XrSwapchainImageVulkanKHR> images(
        imageCount,
        { .type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR }
    );

    result = xrEnumerateSwapchainImages(swapchain, imageCount, &imageCount, (XrSwapchainImageBaseHeader*)images.data());

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain images: " << result << endl;
        return {};
    }

    return images;
}
```

We aren't creating anything here, these images are part of the swapchain, so no cleanup function is required. Finally we can add this to our main:

```
int main(int, char**)
{
    XrInstance instance = createInstance();
    XrDebugUtilsMessengerEXT debugMessenger = createDebugMessenger(instance);
    XrSystemId system = getSystem(instance);

    XrGraphicsRequirementsVulkanKHR graphicsRequirements;
    set<string> instanceExtensions;
    tie(graphicsRequirements, instanceExtensions) = getVulkanInstanceRequirements(instance, system);
    VkInstance vulkanInstance = createVulkanInstance(graphicsRequirements, instanceExtensions);
    VkDebugUtilsMessengerEXT vulkanDebugMessenger = createVulkanDebugMessenger(vulkanInstance);

    VkPhysicalDevice physicalDevice;
    set<string> deviceExtensions;
    tie(physicalDevice, deviceExtensions) = getVulkanDeviceRequirements(instance, system, vulkanInstance);
    int32_t graphicsQueueFamilyIndex = getDeviceQueueFamily(physicalDevice);
    VkDevice device;
    VkQueue queue;
    tie(device, queue) = createDevice(physicalDevice, graphicsQueueFamilyIndex, deviceExtensions);

    XrSession session = createSession(instance, system, vulkanInstance, physicalDevice, device, graphicsQueueFamilyIndex);

    Swapchain* swapchains[eyeCount];
    tie(swapchains[0], swapchains[1]) = createSwapchains(instance, system, session);

    VkRenderPass renderPass = createRenderPass(device, swapchains[0]->format);
    VkCommandPool commandPool = createCommandPool(device, graphicsQueueFamilyIndex);
    VkDescriptorPool descriptorPool = createDescriptorPool(device);
    VkDescriptorSetLayout descriptorSetLayout = createDescriptorSetLayout(device);
    VkShaderModule vertexShader = createShader(device, "vertex.spv");
    VkShaderModule fragmentShader = createShader(device, "fragment.spv");
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    tie(pipelineLayout, pipeline) = createPipeline(device, renderPass, descriptorSetLayout, vertexShader, fragmentShader);

    vector<XrSwapchainImageVulkanKHR> swapchainImages[eyeCount];

    for (size_t i = 0; i < eyeCount; i++)
    {
        swapchainImages[i] = getSwapchainImages(swapchains[i]->swapchain);
    }

    destroyPipeline(device, pipelineLayout, pipeline);
    destroyShader(device, fragmentShader);
    destroyShader(device, vertexShader);
    destroyDescriptorSetLayout(device, descriptorSetLayout);
    destroyDescriptorPool(device, descriptorPool);
    destroyCommandPool(device, commandPool);
    destroyRenderPass(device, renderPass);

    for (size_t i = 0; i < eyeCount; i++)
    {
        delete swapchains[i];
    }

    destroySession(session);

    destroyDevice(device);

    destroyVulkanDebugMessenger(vulkanInstance, vulkanDebugMessenger);
    destroyVulkanInstance(vulkanInstance);

    destroyDebugMessenger(instance, debugMessenger);
    destroyInstance(instance);

    return 0;
}
```

And with that we're almost there, but before we can draw anything to the headset we'll have to start the session.
