This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 1: The OpenXR instance

Much like Vulkan, everything in OpenXR is tied to an "instance". The instance represents a connection to a runtime. Instances are initialized like this:

```
XrInstance createInstance()
{
    XrInstance instance;

    static const char* const applicationName = "OpenXR Example";
    static const unsigned int majorVersion = 0;
    static const unsigned int minorVersion = 1;
    static const unsigned int patchVersion = 0;
    static const char* const extensionNames[] = {
        "XR_KHR_vulkan_enable",
        "XR_KHR_vulkan_enable2"
    };

    XrInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.type = XR_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.createFlags = 0;
    strcpy(instanceCreateInfo.applicationInfo.applicationName, applicationName);
    instanceCreateInfo.applicationInfo.applicationVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    strcpy(instanceCreateInfo.applicationInfo.engineName, applicationName);
    instanceCreateInfo.applicationInfo.engineVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    instanceCreateInfo.applicationInfo.apiVersion = XR_MAKE_VERSION(1, 0, 34);
    instanceCreateInfo.enabledApiLayerCount = 0;
    instanceCreateInfo.enabledApiLayerNames = nullptr;
    instanceCreateInfo.enabledExtensionCount = sizeof(extensionNames) / sizeof(const char*);
    instanceCreateInfo.enabledExtensionNames = extensionNames;

    XrResult result = xrCreateInstance(&instanceCreateInfo, &instance);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR instance: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return instance;
}
```

Most of these inputs are just metadata which the runtime can use to tell the user what application is running, but there are a few things worth noting.

- Be mindful that the length of your chosen strings for `applicationName` and `engineName` cannot exceed `XR_MAX_APPLICATION_NAME_SIZE` and `XR_MAX_ENGINE_NAME_SIZE` respectively.
- `apiVersion` controls which version of the API will be initialized. Usually `XR_CURRENT_API_VERSION` is fine, but I've limited it to `XR_MAKE_VERSION(1, 0, 34)` here because at the time of the last update to this tutorial (April 25th 2024) SteamVR does not support the current version of OpenXR (1.1).
- `enabledExtensionCount` and `enabledExtensionNames` allows you to enable various OpenXR extensions. Here we're just using it to enable the two extensions that let OpenXR talk to Vulkan, but you might want to add other extensions, such as hand tracking and eye tracking.
- You may want to use `enabledApiLayerCount` and `enabledApiLayerNames`, as we'll discuss in the next section.

This creation function goes at the start of our program's `main` function:

```
int main(int, char**)
{
    XrInstance instance = createInstance();

    return 0;
}
```

Now, everything we create in OpenXR needs to be cleaned up again when the program shuts down, so we'll need a second function:

```
void destroyInstance(XrInstance instance)
{
    xrDestroyInstance(instance);
}
```

And then we need to add that to our `main` too:

```
int main(int, char**)
{
    XrInstance instance = createInstance();

    destroyInstance(instance);

    return 0;
}
```

Note that from this point onwards you will need your OpenXR runtime (i.e. SteamVR or Monado XR) to be running when you run the example programs, as the creation of an instance establishes a connection to it. And with that instance initialization is done, aside from one issue which we'll cover in the next section.
