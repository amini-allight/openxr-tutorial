This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 8: Drawing

Now it's time to draw the 3D world into the headset. This part is probably going to the longest and most complex single part so strap in. As was mentioned in the previous part of this tutorial, a swapchain contains at least two images, one of which is being displayed on the screen while at least one another is being drawn to. Because of this the potential exists for the graphics device to be performing drawing work for multiple frames simultaneously, and this is often the case. To handle this we will need to create a type which encapsulates all the various frame-specific resources that are needed to draw that frame. These resources are things that cannot be shared between frames, either because they may contain different input data (multiple frames will often have different object positions, therefore the uniform buffers that store this information must be duplicated) or because they will have different output data written to them (different frames will necessarily have different depth buffers as this information is generated uniquely for each frame). To contain this information we will create a new structure which manages all these objects:

```
struct SwapchainImage
{
    SwapchainImage(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkRenderPass renderPass,
        VkCommandPool commandPool,
        VkDescriptorPool descriptorPool,
        VkDescriptorSetLayout descriptorSetLayout,
        const Swapchain* swapchain,
        XrSwapchainImageVulkanKHR image
    )
        : device(device)
        , commandPool(commandPool)
        , descriptorPool(descriptorPool)
        , image(image)
    {
        VkImageViewCreateInfo imageViewCreateInfo{};
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = image.image;
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = swapchain->format;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        VkResult result = vkCreateImageView(device, &imageViewCreateInfo, nullptr, &imageView);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to create Vulkan image view: " << result << endl;
        }

        VkFramebufferCreateInfo framebufferCreateInfo{};
        framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.renderPass = renderPass;
        framebufferCreateInfo.attachmentCount = 1;
        framebufferCreateInfo.pAttachments = &imageView;
        framebufferCreateInfo.width = swapchain->width;
        framebufferCreateInfo.height = swapchain->height;
        framebufferCreateInfo.layers = 1;

        result = vkCreateFramebuffer(device, &framebufferCreateInfo, nullptr, &framebuffer);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to create Vulkan framebuffer: " << result << endl;
        }

        VkBufferCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        createInfo.size = bufferSize;
        createInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        result = vkCreateBuffer(device, &createInfo, nullptr, &buffer);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to create Vulkan buffer: " << result << endl;
        }

        VkMemoryRequirements requirements;
        vkGetBufferMemoryRequirements(device, buffer, &requirements);

        VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkPhysicalDeviceMemoryProperties properties;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &properties);

        uint32_t memoryTypeIndex = 0;

        for (uint32_t i = 0; i < properties.memoryTypeCount; i++)
        {
            if (!(requirements.memoryTypeBits & (1 << i)))
            {
                continue;
            }

            if ((properties.memoryTypes[i].propertyFlags & flags) != flags)
            {
                continue;
            }

            memoryTypeIndex = i;
            break;
        }

        VkMemoryAllocateInfo allocateInfo{};
        allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
        allocateInfo.allocationSize = requirements.size;
        allocateInfo.memoryTypeIndex = memoryTypeIndex;

        result = vkAllocateMemory(device, &allocateInfo, nullptr, &memory);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to allocate Vulkan memory: " << result << endl;
        }

        vkBindBufferMemory(device, buffer, memory, 0);

        VkCommandBufferAllocateInfo commandBufferAllocateInfo{};
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.commandPool = commandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1;

        result = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to allocate Vulkan command buffers: " << result << endl;
        }

        VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
        descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        descriptorSetAllocateInfo.descriptorPool = descriptorPool;
        descriptorSetAllocateInfo.descriptorSetCount = 1;
        descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

        result = vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &descriptorSet);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to allocate Vulkan descriptor sets: " << result << endl;
        }

        VkDescriptorBufferInfo descriptorBufferInfo{};
        descriptorBufferInfo.buffer = buffer;
        descriptorBufferInfo.offset = 0;
        descriptorBufferInfo.range = VK_WHOLE_SIZE;

        VkWriteDescriptorSet descriptorWrite{};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = descriptorSet;
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.pBufferInfo = &descriptorBufferInfo;

        vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
    }

    ~SwapchainImage()
    {
        vkFreeDescriptorSets(device, descriptorPool, 1, &descriptorSet);
        vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
        vkDestroyBuffer(device, buffer, nullptr);
        vkFreeMemory(device, memory, nullptr);
        vkDestroyFramebuffer(device, framebuffer, nullptr);
        vkDestroyImageView(device, imageView, nullptr);
    }

    XrSwapchainImageVulkanKHR image;
    VkImageView imageView;
    VkFramebuffer framebuffer;
    VkDeviceMemory memory;
    VkBuffer buffer;
    VkCommandBuffer commandBuffer;
    VkDescriptorSet descriptorSet;

private:
    VkDevice device;
    VkCommandPool commandPool;
    VkDescriptorPool descriptorPool;
};
```

Because I don't want to teach any bad habits I want to be clear: this is **not** how you should create a uniform buffer. Vulkan does provide some very basic memory allocation, but only for a couple large blocks, and I'm using that here. For the large number of small blocks found in most real-world applications those large blocks need to be subdivided using some memory management algorithm, which Vulkan leaves as an exercise for the programmer. Good memory managers can be very complicated and run to tens of thousands of lines so the typical solution is to use an off-the-shelf solution such as [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator).

Then we will create one of these structures for each of our swapchain images in our `main` function:

```
vector<SwapchainImage*> wrappedSwapchainImages[eyeCount];

for (size_t i = 0; i < eyeCount; i++)
{
    wrappedSwapchainImages[i] = vector<SwapchainImage*>(swapchainImages[i].size(), nullptr);

    for (size_t j = 0; j < wrappedSwapchainImages[i].size(); j++)
    {
        wrappedSwapchainImages[i][j] = new SwapchainImage(
            physicalDevice,
            device,
            renderPass,
            commandPool,
            descriptorPool,
            descriptorSetLayout,
            swapchains[i],
            swapchainImages[i][j]
        );
    }
}
```

Now the next thing we will need is a "reference space". This represents a particular reference point we can find the position and rotation of other things relative to. In this case we'll define a "stage" reference space. This space will give us a basic 6DOF reference space: moving the headset or a controller upwards in real life will move it upwards in this space.

```
XrSpace createSpace(XrSession session)
{
    XrSpace space;

    XrReferenceSpaceCreateInfo spaceCreateInfo{};
    spaceCreateInfo.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO;
    spaceCreateInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    spaceCreateInfo.poseInReferenceSpace = { { 0, 0, 0, 1 }, { 0, 0, 0 } };

    XrResult result = xrCreateReferenceSpace(session, &spaceCreateInfo, &space);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create space: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return space;
}
```

You can use `poseInReferenceSpace` here to move the play space through the game's world space. For example if we set it to `{ { 0, 0, 1, 0 }, { 0, 100, 0 } }` (upside down, 100 m from the origin) and then queried the headset's position we would find that moving the headset upwards in the room now moves it downwards in the game world, and a headset at the center of the play space now appears to be 100 m from the center of the game world. As usual we need to clean this object up:

```
void destroySpace(XrSpace space)
{
    xrDestroySpace(space);
}
```

Now within the `running` check below that we left unfinished in the previous part we need to wait for a frame to be ready, this is quite easy:

```
if (running)
{
    XrFrameWaitInfo frameWaitInfo{};
    frameWaitInfo.type = XR_TYPE_FRAME_WAIT_INFO;

    XrFrameState frameState{};
    frameState.type = XR_TYPE_FRAME_STATE;

    XrResult result = xrWaitFrame(session, &frameWaitInfo, &frameState);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to wait for frame: " << result << endl;
        break;
    }

    if (!frameState.shouldRender)
    {
        continue;
    }

    quit = !render(
        session,
        swapchains,
        wrappedSwapchainImages,
        space,
        frameState.predictedDisplayTime,
        device,
        queue,
        renderPass,
        pipelineLayout,
        pipeline
    );
}
```

`XrFrameWaitInfo` only exists to support extension-specific functionality, so it doesn't require any configuration. It is possible that the runtime will tell us not to render anything, in which case we'll go back around and try again. Now that we know the runtime expects a frame from us, we can get to work on rendering one:

```
bool render(
    XrSession session,
    Swapchain* swapchains[2],
    vector<SwapchainImage*> swapchainImages[2],
    XrSpace space,
    XrTime predictedDisplayTime,
    VkDevice device,
    VkQueue queue,
    VkRenderPass renderPass,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
)
{
    XrFrameBeginInfo beginFrameInfo{};
    beginFrameInfo.type = XR_TYPE_FRAME_BEGIN_INFO;

    XrResult result = xrBeginFrame(session, &beginFrameInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to begin frame: " << result << endl;
        return false;
    }
}
```

Much like `XrFrameWaitInfo`, `XrFrameBeginInfo` doesn't do anything unless certain extensions are used. Calling `xrBeginFrame` here signals our intent to render something for this frame. But to render something we need to know where the cameras (i.e. the user's headset) lie within our scene, and that's what this next function will tell us:

```
XrViewLocateInfo viewLocateInfo{};
viewLocateInfo.type = XR_TYPE_VIEW_LOCATE_INFO;
viewLocateInfo.viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
viewLocateInfo.displayTime = predictedDisplayTime;
viewLocateInfo.space = space;

XrViewState viewState{};
viewState.type = XR_TYPE_VIEW_STATE;

uint32_t viewCount = eyeCount;
vector<XrView> views(
    viewCount,
    { .type = XR_TYPE_VIEW }
);

result = xrLocateViews(
    session,
    &viewLocateInfo,
    &viewState,
    viewCount,
    &viewCount,
    views.data()
);

if (result != XR_SUCCESS)
{
    cerr << "Failed to locate views: " << result << endl;
    return false;
}
```

We specify `viewConfigurationType` to be `XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO` because we'll be rendering to a headset. We set `displayTime` to `predictedDisplayTime`, the value we got from `xrWaitFrame` to tell the runtime we want its estimate of where the headset _will_ be by the time this frame is actually shown to the user. This decreases perceived latency and so helps alleviate motion sickness. Finally we set `space` to our reference space from earlier. This is the space within which the headset will be located, in this case we'll be finding where the headset is relative to the origin of the play space, plus any transform applied to that via `poseInReferenceSpace`.

From here on out we need to deal with two separate renders, one for each eye. We'll create a new function, `renderEye` and call it twice like so:

```
for (size_t i = 0; i < eyeCount; i++)
{
    bool ok = renderEye(
        swapchains[i],
        swapchainImages[i],
        views[i],
        device,
        queue,
        renderPass,
        pipelineLayout,
        pipeline
    );

    if (!ok)
    {
        return false;
    }
}
```

To start with this function needs to acquire an image from the swapchain, this will tell us which image the runtime wants us to use for this frame:

```
bool renderEye(
    Swapchain* swapchain,
    const vector<SwapchainImage*>& images,
    XrView view,
    VkDevice device,
    VkQueue queue,
    VkRenderPass renderPass,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
)
{
    XrSwapchainImageAcquireInfo acquireImageInfo{};
    acquireImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO;

    uint32_t activeIndex;

    XrResult result = xrAcquireSwapchainImage(swapchain->swapchain, &acquireImageInfo, &activeIndex);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to acquire swapchain image: " << result << endl;
        return false;
    }
}
```

`XrSwapchainImageAcquireInfo` is another of those structures that is only used by extensions. Now we need to wait for the image to be ready, as the runtime is still potentially displaying this image when we acquire it, we'll do that like so:

```
XrSwapchainImageWaitInfo waitImageInfo{};
waitImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO;
waitImageInfo.timeout = numeric_limits<int64_t>::max();

result = xrWaitSwapchainImage(swapchain->swapchain, &waitImageInfo);

if (result != XR_SUCCESS)
{
    cerr << "Failed to wait for swapchain image: " << result << endl;
    return false;
}

const SwapchainImage* image = images[activeIndex];
```

The only parameter here is `timeout` which controls how long we want to wait, we'll default to the maximum possible duration. And now at long last we can render something into this image! I'm not going to explain this in too much detail, pretty much everything here should be familiar to anyone who's used Vulkan before.

```
float* data;
VkResult vkResult = vkMapMemory(device, image->memory, 0, VK_WHOLE_SIZE, 0, (void**)&data);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to map Vulkan memory: " << result << endl;
}

float angleWidth = tan(view.fov.angleRight) - tan(view.fov.angleLeft);
float angleHeight = tan(view.fov.angleDown) - tan(view.fov.angleUp);

float projectionMatrix[4][4]{0};

projectionMatrix[0][0] = 2.0f / angleWidth;
projectionMatrix[2][0] = (tan(view.fov.angleRight) + tan(view.fov.angleLeft)) / angleWidth;
projectionMatrix[1][1] = 2.0f / angleHeight;
projectionMatrix[2][1] = (tan(view.fov.angleUp) + tan(view.fov.angleDown)) / angleHeight;
projectionMatrix[2][2] = -farDistance / (farDistance - nearDistance);
projectionMatrix[3][2] = -(farDistance * nearDistance) / (farDistance - nearDistance);
projectionMatrix[2][3] = -1;

glm::mat4 viewMatrix = glm::inverse(
    glm::translate(glm::mat4(1.0f), glm::vec3(view.pose.position.x, view.pose.position.y, view.pose.position.z))
    * glm::mat4_cast(glm::quat(view.pose.orientation.w, view.pose.orientation.x, view.pose.orientation.y, view.pose.orientation.z))
);

float modelMatrix[4][4]{
    { 1, 0, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 1, 0 },
    { 0, 0, 0, 1 }
};

memcpy(data, projectionMatrix, sizeof(float) * 4 * 4);
memcpy(data + (4 * 4), glm::value_ptr(viewMatrix), sizeof(float) * 4 * 4);
memcpy(data + (4 * 4) * 2, modelMatrix, sizeof(float) * 4 * 4);

vkUnmapMemory(device, image->memory);

VkCommandBufferBeginInfo beginInfo{};
beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

vkResult = vkBeginCommandBuffer(image->commandBuffer, &beginInfo);

VkClearValue clearValue{};
clearValue.color = { { 0.0f, 0.0f, 0.0f, 1.0f } };

VkRenderPassBeginInfo beginRenderPassInfo{};
beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
beginRenderPassInfo.renderPass = renderPass;
beginRenderPassInfo.framebuffer = image->framebuffer;
beginRenderPassInfo.renderArea = {
    { 0, 0 },
    { (uint32_t)swapchain->width, (uint32_t)swapchain->height }
};
beginRenderPassInfo.clearValueCount = 1;
beginRenderPassInfo.pClearValues = &clearValue;

vkCmdBeginRenderPass(image->commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

VkViewport viewport = {
    0, 0,
    (float)swapchain->width, (float)swapchain->height,
    0, 1
};

vkCmdSetViewport(image->commandBuffer, 0, 1, &viewport);

VkRect2D scissor = {
    { 0, 0 },
    { (uint32_t)swapchain->width, (uint32_t)swapchain->height }
};

vkCmdSetScissor(image->commandBuffer, 0, 1, &scissor);

vkCmdBindPipeline(image->commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

vkCmdBindDescriptorSets(image->commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &image->descriptorSet, 0, nullptr);

vkCmdDraw(image->commandBuffer, 3, 1, 0, 0);

vkCmdEndRenderPass(image->commandBuffer);

vkResult = vkEndCommandBuffer(image->commandBuffer);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to end Vulkan command buffer: " << vkResult << endl;
    return false;
}

VkPipelineStageFlags stageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

VkSubmitInfo submitInfo{};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pWaitDstStageMask = &stageMask;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &image->commandBuffer;

vkResult = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to submit Vulkan command buffer: " << result << endl;
    return false;
}
```

We're using a new library here, GLM, to handle some of the more unpleasant 3D math for us. We'll have to add includes for that to the top of our program:

```
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
```

And then as the final step we release the image, signaling to OpenXR that we don't need it anymore.

```
XrSwapchainImageReleaseInfo releaseImageInfo{};
releaseImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO;

result = xrReleaseSwapchainImage(swapchain->swapchain, &releaseImageInfo);

if (result != XR_SUCCESS)
{
    cerr << "Failed to release swapchain image: " << result << endl;
    return false;
}
```

Once again `XrSwapchainImageReleaseInfo` is a structure only used by extensions. To complete our functions we return to the `render` function and add this after rendering both eyes:

```
XrCompositionLayerProjectionView projectedViews[2]{};

for (size_t i = 0; i < eyeCount; i++)
{
    projectedViews[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
    projectedViews[i].pose = views[i].pose;
    projectedViews[i].fov = views[i].fov;
    projectedViews[i].subImage = {
        swapchains[i]->swapchain,
        {
            { 0, 0 },
            { (int32_t)swapchains[i]->width, (int32_t)swapchains[i]->height }
        },
        0
    };
}

XrCompositionLayerProjection layer{};
layer.type = XR_TYPE_COMPOSITION_LAYER_PROJECTION;
layer.space = space;
layer.viewCount = eyeCount;
layer.views = projectedViews;

auto pLayer = (const XrCompositionLayerBaseHeader*)&layer;

XrFrameEndInfo endFrameInfo{};
endFrameInfo.type = XR_TYPE_FRAME_END_INFO;
endFrameInfo.displayTime = predictedDisplayTime;
endFrameInfo.environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE;
endFrameInfo.layerCount = 1;
endFrameInfo.layers = &pLayer;

result = xrEndFrame(session, &endFrameInfo);

if (result != XR_SUCCESS)
{
    cerr << "Failed to end frame: " << result << endl;
    return false;
}
```

Here we tell the runtime how we'd like the images we've just drawn to be displayed. We create two `XrCompositionLayerProjectionView` objects and populate them with the transform and FOV of their respective eyes, plus some basic details of the swapchain. When the combine these with the room space object in a `XrCompositionLayerProjection` structure. This structure uses the same technique we used earlier with multiple specialized structure types and one base structure type containing a generic byte array. We'll create a pointer to the base structure type here because that's what the next part expects. Finally we supply this pointer to `xrEndFrame` alongside an indication of how we want this content to be mixed with other things that may be in the headset (i.e. other applications, system overlays, the real world in the case of an AR device), we're using `XR_ENVIRONMENT_BLEND_MODE_OPAQUE` because this is going to be a traditional VR application. We also need to supply the time at which we want the frame to be displayed, our camera positions were predicted for `predictedDisplayTime` so we'll use that time again here.

There's one last thing we need to do. Now that we actually use our Vulkan objects in command buffers supplied to `vkQueueSubmit` we have to make sure those command buffers have finished executing before we can shut down the program. To do this we add this after our main loop:

```
VkResult result = vkDeviceWaitIdle(device);

if (result != VK_SUCCESS)
{
    cerr << "Failed to wait for device to idle: " << result << endl;
}
```

If you start the program and put your headset on you should see a triangle sitting at the origin (probably in the center of your floor). There's only one thing left to do, enabling you to interact with it, which will be the topic of our next and final part.
