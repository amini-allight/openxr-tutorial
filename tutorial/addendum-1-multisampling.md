This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Addendum 1: Multisampling

Since writing my OpenXR tutorial two issues with it have nagged at me. One is the lack of anything covering how to implement multisample anti-aliasing correctly with OpenXR and the other is the fact that the tutorial uses two swapchains and two entire GPU command dispatches when there is a much cleaner and faster way of using OpenXR that utilizes the GPU's multiview capability to cut the number of draw calls in half. This will be the first of two addenda to the original tutorial covering these topics, starting with MSAA.

MSAA has kind of fallen out of favor in recent years, mainly because it's incompatible with deferred rendering and also a little because it needlessly complicates all the per-pixel operations modern games want to do. But there are still good reasons to use it (it doesn't muddy images the way alternative anti-aliasing approaches tend to) and it's supported by OpenXR, so I wanted to cover its use here.

MSAA works by rendering into a multisampled image that is actually a stack of 2/4/8/etc. images. The first image has every pixel filled while the others only have their pixels filled along triangle edges in the image in order to increase resolution in specifically those areas. The multisampled image is then "resolved" into a normal monosample image for final display, post-processing and so on.

There are two ways to do this with OpenXR. You can either submit still-multisampled images back to OpenXR and leave the resolution into monosampled images up to the runtime or you can resolve the image yourself and submit the monosample version. I've provided examples of both and I'll be going through them in turn, starting with submitting still-multisampled images directly to the runtime because it's the simpler of the two.

## Approach \#1: Runtime Resolution

To start with, we need to get the runtime's recommended level of anti-aliasing. We can do this by accessing the `recommendedSwapchainSampleCount` property of `XrViewConfigurationView` (which we retrieve from `xrEnumeratedViewConfigurationViews`) which we had ignored up to now. We can pass that into the creation of our swapchain to tell the runtime we want images to be created with additional samples for multisampling, like so:

```cpp
XrSwapchain swapchains[eyeCount];

for (size_t i = 0; i < eyeCount; i++)
{
    XrSwapchainCreateInfo swapchainCreateInfo{};
    swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
    swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCreateInfo.format = chosenFormat;
    swapchainCreateInfo.sampleCount = max((VkSampleCountFlagBits)configViews[i].recommendedSwapchainSampleCount, min(VK_SAMPLE_COUNT_2_BIT, (VkSampleCountFlagBits)configViews[i].maxSwapchainSampleCount));
    swapchainCreateInfo.width = configViews[i].recommendedImageRectWidth;
    swapchainCreateInfo.height = configViews[i].recommendedImageRectHeight;
    swapchainCreateInfo.faceCount = 1;
    swapchainCreateInfo.arraySize = 1;
    swapchainCreateInfo.mipCount = 1;

    result = xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create swapchain: " << result << endl;
        return { nullptr, nullptr };
    }
}

return {
    new Swapchain(
        swapchains[0],
        (VkFormat)chosenFormat,
        max(
            (VkSampleCountFlagBits)configViews[0].recommendedSwapchainSampleCount,
            min(VK_SAMPLE_COUNT_2_BIT, (VkSampleCountFlagBits)configViews[0].maxSwapchainSampleCount)
        ),
        configViews[0].recommendedImageRectWidth,
        configViews[0].recommendedImageRectHeight
    ),
    new Swapchain(
        swapchains[1],
        (VkFormat)chosenFormat,
        max(
            (VkSampleCountFlagBits)configViews[1].recommendedSwapchainSampleCount,
            min(VK_SAMPLE_COUNT_2_BIT, (VkSampleCountFlagBits)configViews[1].maxSwapchainSampleCount)
        ),
        configViews[1].recommendedImageRectWidth,
        configViews[1].recommendedImageRectHeight
    )
};
```

We also pass the sample count to our `Swapchain` constructor and store it in a member variable of that structure so we can refer back to it later when constructing other objects. You may notice I'm forcing the sample count to be at least two (provided the system supports it) with `std::max`. I'm doing this because SteamVR always reports a recommended sample count of 1, meaning this example wouldn't be doing any multisampling at all otherwise. Next we will need to inform the GPU that it's going to be rendering to multisampled images. This is done in two places: the `VkRenderPass` and the `VkPipeline`. For the render pass that looks like this:

```cpp
VkRenderPass createRenderPass(VkDevice device, VkFormat format, VkSampleCountFlagBits sampleCount)
{
    VkRenderPass renderPass;

    VkAttachmentDescription attachment{};
    attachment.format = format;
    attachment.samples = sampleCount;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference attachmentRef{};
    attachmentRef.attachment = 0;
    attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &attachmentRef;

    VkRenderPassCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    createInfo.flags = 0;
    createInfo.attachmentCount = 1;
    createInfo.pAttachments = &attachment;
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpass;

    VkResult result = vkCreateRenderPass(device, &createInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan render pass: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return renderPass;
}
```

Setting the `VkAttachmentDescription::samples` value to the value we acquired from `XrViewConfigurationView::recommendedSwapchainSampleCount` is the only change. And for the pipeline:

```cpp
VkPipelineMultisampleStateCreateInfo multisampleStage{};
multisampleStage.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
multisampleStage.rasterizationSamples = sampleCount;
multisampleStage.sampleShadingEnable = false;
multisampleStage.minSampleShading = 0.25;
```

Again setting `VkPipelineMultisampleStateCreateInfo::rasterizationSamples` is the only difference. And that's really it. The `VkImageView` and `VkFramebuffer` will implicitly be multisampled because the underlying `VkImage` provided by the runtime is and the combination of the `VkRenderPass` and `VkPipeline` will render to it with multisampling enabled.

## Approach \#2: Application Resolution

The second approach is more complicated. For this approach we read the `recommendedSwapchainSampleCount` value as before but now we only store the value in our `Swapchain` object, we leave `XrSwapchainCreateInfo::sampleCount` set to `VK_SAMPLE_COUNT_1_BIT` because we will be submitting pre-resolved images:

```cpp
XrSwapchain swapchains[eyeCount];

for (size_t i = 0; i < eyeCount; i++)
{
    XrSwapchainCreateInfo swapchainCreateInfo{};
    swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
    swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCreateInfo.format = chosenFormat;
    swapchainCreateInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
    swapchainCreateInfo.width = configViews[i].recommendedImageRectWidth;
    swapchainCreateInfo.height = configViews[i].recommendedImageRectHeight;
    swapchainCreateInfo.faceCount = 1;
    swapchainCreateInfo.arraySize = 1;
    swapchainCreateInfo.mipCount = 1;

    result = xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create swapchain: " << result << endl;
        return { nullptr, nullptr };
    }
}

return {
    new Swapchain(
        swapchains[0],
        (VkFormat)chosenFormat,
        max(
            (VkSampleCountFlagBits)configViews[0].recommendedSwapchainSampleCount,
            min(VK_SAMPLE_COUNT_2_BIT, (VkSampleCountFlagBits)configViews[0].maxSwapchainSampleCount)
        ),
        configViews[0].recommendedImageRectWidth,
        configViews[0].recommendedImageRectHeight
    ),
    new Swapchain(
        swapchains[1],
        (VkFormat)chosenFormat,
        max(
            (VkSampleCountFlagBits)configViews[1].recommendedSwapchainSampleCount,
            min(VK_SAMPLE_COUNT_2_BIT, (VkSampleCountFlagBits)configViews[1].maxSwapchainSampleCount)
        ),
        configViews[1].recommendedImageRectWidth,
        configViews[1].recommendedImageRectHeight
    )
};
```

Once again we need to feed this value into the creation of the `VkRenderPass`, but this is much more complex than before:

```cpp
VkRenderPass createRenderPass(VkDevice device, VkFormat format, VkSampleCountFlagBits sampleCount)
{
    VkRenderPass renderPass;

    VkAttachmentDescription attachment{};
    attachment.format = format;
    attachment.samples = sampleCount;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference attachmentRef{};
    attachmentRef.attachment = 0;
    attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription resolveAttachment{};
    resolveAttachment.format = format;
    resolveAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    resolveAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    resolveAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    resolveAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    resolveAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference resolveAttachmentRef{};
    resolveAttachmentRef.attachment = 1;
    resolveAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &attachmentRef;
    subpass.pResolveAttachments = &resolveAttachmentRef;

    VkAttachmentDescription attachments[] = {
        attachment,
        resolveAttachment
    };

    VkRenderPassCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    createInfo.flags = 0;
    createInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription);
    createInfo.pAttachments = attachments;
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpass;

    VkResult result = vkCreateRenderPass(device, &createInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan render pass: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return renderPass;
}
```

We now create two attachments instead of one. The first is the multisampled attachment. It has a sample count greater than 1 and the store operation `VK_ATTACHMENT_STORE_OP_DONT_CARE` because the value won't actually be stored long-term, instead it will be immediately resolved into the second attachment. The second attachment is where the final image ends up, after the multiple samples of the first image have been blended together. It has a sample count of 1 and the load operation `VK_ATTACHMENT_LOAD_DONT_CARE` because the contents of this image will always be overwritten by multisample resolution. In the `VkSubpassDescription` we define the second attachment as a resolve attachment which tells the GPU what to do with it, then we supply both attachments to the creation of the actual render pass.

We also use the sample count in the pipeline's creation as before:

```cpp
VkPipelineMultisampleStateCreateInfo multisampleStage{};
multisampleStage.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
multisampleStage.rasterizationSamples = sampleCount;
multisampleStage.sampleShadingEnable = false;
multisampleStage.minSampleShading = 0.25;
```

Finally we need to alter the structure of our `SwapchainImage` structure: the render pass expects two attachment so the framebuffer must have two `VkImageView`s attached to it as well. Here's the new constructor:

```cpp
VkImageCreateInfo imageCreateInfo{};
imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
imageCreateInfo.extent = { swapchain->width, swapchain->height, 1 };
imageCreateInfo.mipLevels = 1;
imageCreateInfo.arrayLayers = 1;
imageCreateInfo.format = swapchain->format;
imageCreateInfo.samples = swapchain->sampleCount;
imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
imageCreateInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

VmaAllocationCreateInfo imageAllocateInfo{};
imageAllocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

VkResult result = vmaCreateImage(allocator, &imageCreateInfo, &imageAllocateInfo, &image.image, &image.allocation, nullptr);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan image: " << result << endl;
}

VkImageViewCreateInfo imageViewCreateInfo{};
imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
imageViewCreateInfo.image = image.image;
imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
imageViewCreateInfo.format = swapchain->format;
imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
imageViewCreateInfo.subresourceRange.levelCount = 1;
imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
imageViewCreateInfo.subresourceRange.layerCount = 1;

result = vkCreateImageView(device, &imageViewCreateInfo, nullptr, &imageView);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan image view: " << result << endl;
}

VkImageViewCreateInfo resolveImageViewCreateInfo{};
resolveImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
resolveImageViewCreateInfo.image = resolveImage.image;
resolveImageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
resolveImageViewCreateInfo.format = swapchain->format;
resolveImageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
resolveImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
resolveImageViewCreateInfo.subresourceRange.levelCount = 1;
resolveImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
resolveImageViewCreateInfo.subresourceRange.layerCount = 1;

result = vkCreateImageView(device, &resolveImageViewCreateInfo, nullptr, &resolveImageView);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan image view: " << result << endl;
}

VkImageView attachments[] = {
    imageView,
    resolveImageView
};

VkFramebufferCreateInfo framebufferCreateInfo{};
framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
framebufferCreateInfo.renderPass = renderPass;
framebufferCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkImageView);
framebufferCreateInfo.pAttachments = attachments;
framebufferCreateInfo.width = swapchain->width;
framebufferCreateInfo.height = swapchain->height;
framebufferCreateInfo.layers = 1;

result = vkCreateFramebuffer(device, &framebufferCreateInfo, nullptr, &framebuffer);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan framebuffer: " << result << endl;
}

VkBufferCreateInfo bufferCreateInfo{};
bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
bufferCreateInfo.size = bufferSize;
bufferCreateInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

VmaAllocationCreateInfo allocInfo{};
allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

result = vmaCreateBuffer(allocator, &bufferCreateInfo, &allocInfo, &buffer.buffer, &buffer.allocation, nullptr);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan buffer: " << result << endl;
}

VkCommandBufferAllocateInfo commandBufferAllocateInfo{};
commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
commandBufferAllocateInfo.commandPool = commandPool;
commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
commandBufferAllocateInfo.commandBufferCount = 1;

result = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);

if (result != VK_SUCCESS)
{
    cerr << "Failed to allocate Vulkan command buffers: " << result << endl;
}

VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{};
descriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
descriptorSetAllocateInfo.descriptorPool = descriptorPool;
descriptorSetAllocateInfo.descriptorSetCount = 1;
descriptorSetAllocateInfo.pSetLayouts = &descriptorSetLayout;

result = vkAllocateDescriptorSets(device, &descriptorSetAllocateInfo, &descriptorSet);

if (result != VK_SUCCESS)
{
    cerr << "Failed to allocate Vulkan descriptor sets: " << result << endl;
}

VkDescriptorBufferInfo descriptorBufferInfo{};
descriptorBufferInfo.buffer = buffer.buffer;
descriptorBufferInfo.offset = 0;
descriptorBufferInfo.range = VK_WHOLE_SIZE;

VkWriteDescriptorSet descriptorWrite{};
descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
descriptorWrite.dstSet = descriptorSet;
descriptorWrite.dstBinding = 0;
descriptorWrite.dstArrayElement = 0;
descriptorWrite.descriptorCount = 1;
descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
descriptorWrite.pBufferInfo = &descriptorBufferInfo;

vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
```

We use the image that OpenXR supplied as the resolve attachment (it will have 1 sample because of the value we set for `XrSwapchainCreateInfo::sampleCount`) and create an image of our own to use as the multisampled attachment. This image has all the properties you'd expect: its format, width, height and sample count match those recommended by the OpenXR runtime and we tell Vulkan we intend to use it as a color attachment. We then create two image views, one for the image we just created and one for the image OpenXR supplied. It's important we supply them to `VkFramebufferCreateInfo` in the same order that the render pass expects them, in this case that's multisampled first, resolved second.

Note that I have switched to using [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator) here, unlike in the other tutorial parts, because I was getting concerned about the size and number of allocations being performed without it. This is, as I acknowledge in [part 8](part-8.md) much better practice and something you should always be doing in real production code (or using an alternative library that does the same thing).

But putting aside issues of memory management that's this approach complete as well, the program renders into a multisampled image of our own creation, resolves that into a monosample image created by the runtime and then hands it back to the runtime. This is much more powerful than the first version because the potential now exists for you to insert arbitrary post-processing steps after the call to `vkCmdEndRenderPass` and before `vkEndCommandBuffer` without those steps needing to operate on complex multisampled images.
