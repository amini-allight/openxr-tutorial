This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 7: The Event Loop

In order to render things in OpenXR we need to begin the session, and we can't begin the session until the OpenXR runtime instructs us to. This instruction arrives via the OpenXR event system, which is the topic of this piece. We'll construct a main loop, a common feature of most realtime applications, that will handle OpenXR events for us and move in and out of different states based on them. To start with, we'll need the structure to support receiving interrupts to leave our main loop:

```
static bool quit = false;

void onInterrupt(int)
{
    quit = true;
}
```

Then a basic main loop:

```
signal(SIGINT, onInterrupt);

bool running = false;
while (!quit)
{

}
```

Then we poll for an event inside the while loop:

```
XrEventDataBuffer eventData{};
eventData.type = XR_TYPE_EVENT_DATA_BUFFER;

XrResult result = xrPollEvent(instance, &eventData);
```

This `XrEventDataBuffer` structure is a bit unlike the structures we've encountered before. It looks like a regular OpenXR structure and starts the same way with a `type` field and `next` pointer (unused here), but the remainder of the struct is basic byte array, large enough to contain the member fields of any of the various other `XrEventData*` structs. To it we first set `type` to be `XR_TYPE_EVENT_DATA_BUFFER`, then call `xrPollEvent`. This presents us with three possibilities:

```
if (result == XR_EVENT_UNAVAILABLE)
{
    if (running)
    {

    }
}
else if (result != XR_SUCCESS)
{
    cerr << "Failed to poll events: " << result << endl;
    break;
}
else
{

}
```

If we get the status code `XR_EVENT_UNAVAILABLE` then there are no events available in the queue right now and we can proceed do other work. This is where our rendering code will be called, but we're leaving it empty for now. Note that we need to handle this before we check `result != XR_SUCCESS` because `XR_EVENT_UNAVAILABLE` isn't `XR_SUCCESS`, yet doesn't warrant shutting down the program. The third option is we do get `XR_SUCCESS`, in which case we received a valid event and need to process it. We'll process the events like so:


```
switch (eventData.type)
{
default :
    cerr << "Unknown event type received: " << eventData.type << endl;
    break;
case XR_TYPE_EVENT_DATA_EVENTS_LOST :
    cerr << "Event queue overflowed and events were lost." << endl;
    break;
case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING :
    cout << "OpenXR instance is shutting down." << endl;
    quit = true;
    break;
case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED :
    cout << "The interaction profile has changed." << endl;
    break;
case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING :
    cout << "The reference space is changing." << endl;
    break;
case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED :
{
    auto event = (XrEventDataSessionStateChanged*)&eventData;

    switch (event->state)
    {
    case XR_SESSION_STATE_UNKNOWN :
    case XR_SESSION_STATE_MAX_ENUM :
        cerr << "Unknown session state entered: " << event->state << endl;
        break;
    case XR_SESSION_STATE_IDLE :
        running = false;
        break;
    case XR_SESSION_STATE_READY :
    {
        XrSessionBeginInfo sessionBeginInfo{};
        sessionBeginInfo.type = XR_TYPE_SESSION_BEGIN_INFO;
        sessionBeginInfo.primaryViewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

        result = xrBeginSession(session, &sessionBeginInfo);

        if (result != XR_SUCCESS)
        {
            cerr << "Failed to begin session: " << result << endl;
        }

        running = true;
        break;
    }
    case XR_SESSION_STATE_SYNCHRONIZED :
    case XR_SESSION_STATE_VISIBLE :
    case XR_SESSION_STATE_FOCUSED :
        running = true;
        break;
    case XR_SESSION_STATE_STOPPING :
        result = xrEndSession(session);

        if (result != XR_SUCCESS)
        {
            cerr << "Failed to end session: " << result << endl;
        }
        break;
    case XR_SESSION_STATE_LOSS_PENDING :
        cout << "OpenXR session is shutting down." << endl;
        quit = true;
        break;
    case XR_SESSION_STATE_EXITING :
        cout << "OpenXR runtime requested shutdown." << endl;
        quit = true;
        break;
    }
    break;
}
}
```

Note that `eventData.type` has now been changed from its default value to a more specific value that indicates which type of event it contains. Here we're processing all the event types defined by the base OpenXR standard, however there are more event types associated with various extensions which you may wish to handle when using those extensions. You can read the specifics of each of these events in the OpenXR standard itself, but mostly they're pretty self-explanatory. The one I want to focus on is `XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED` because this is the one that tells us when we should render frames and when we should begin and end our session. First we cast the generic event structure to a more specific `XrEventDataSessionStateChanged` type, and then we handle each of the possible states:

- `XR_SESSION_STATE_IDLE` the session is in its initial state.
- `XR_SESSION_STATE_READY` the session is ready for us to call `xrBeginSession` and start rendering.
- `XR_SESSION_STATE_SYNCHRONIZED`, `XR_SESSION_STATE_VISIBLE` and `XR_SESSION_STATE_FOCUSED` in these states the session is ready to receive frames and we can begin rendering.
- `XR_SESSION_STATE_STOPPING` the session is stopping and wants us to call `xrEndSession` and stop rendering.
- `XR_SESSION_STATE_LOSS_PENDING` the session is being lost. We simply take this as a signal to shut down but it is possible to attempt to recreate the session when receiving this message. The same applies for recreating the entire instance upon receipt of `XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING`.
- `XR_SESSION_STATE_EXITING` the runtime has requested that we shut down and not attempt to restart.

And that's it for the event loop, if you open the program now it should sit there rather than immediately closing as it did previous to this, as it's sitting inside of the event loop. Next time, we will finally get to draw something to the headset!
