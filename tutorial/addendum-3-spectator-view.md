This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Addendum 3: Spectator View

I was only going to do two addenda to my OpenXR tutorial but then somebody reminded me of another ancillary topic that I should cover: spectator view. This is a feature common to most VR applications for PC where a window opens on the desktop displaying what the person using the headset sees. This can be useful for recording the VR view, allowing people to spectate the VR session or debugging issues. It presents some unique challenges due to the need to render to two different outputs simultaneously.

As with my previous addenda I'm going to have to make some concessions for brevity and start from another tutorial part as a base, assuming you're already familiar with all of the code within, then discuss the changes I've made to create this new version. In this case we're starting from the base found in Addendum 2: Single Swapchain. This is quite similar to Part 9 from the end of the tutorial, differing only in a few ways necessary to use one swapchain instead of two. Also, like in addendum 1, I am using [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator) here, unlike in the other tutorial parts, because I was getting concerned about the size and number of allocations being performed without it. This is, as I acknowledge in [part 8](part-8.md) much better practice and something you should always be doing in real production code (or using an alternative library that does the same thing).

We're going to be using SDL to initialize a window that will form the basis of our spectator view. Starting from the beginning of the main function we add this code:

```cpp
int error = SDL_Init(SDL_INIT_VIDEO);

if (error)
{
    cerr << "Failed to initialize SDL: " << SDL_GetError() << endl;
    return 1;
}

SDL_Window* window = SDL_CreateWindow(
    applicationName,
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    windowWidth,
    windowHeight,
    SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN
);

if (!window)
{
    cerr << "Failed to create window: " << SDL_GetError() << endl;
    return 1;
}
```

This should be pretty familiar to anyone who's used SDL before but just quickly: `SDL_Init` initializes the SDL library, and `SDL_INIT_VIDEO` tells it to initialize the video subsystem, which is only part we're going to need here. `SDL_CreateWindow` then creates a window that will be centered in our screen, bear the name of our application and have a default width and height set to some constants we specify. The final argument ORs some flags together to make sure the window is visible, resizable by dragging on the edges and supports a Vulkan surface being created from it.

The next thing we do needs to come before we initialize our `VkInstance` and that's finding out which extensions SDL wants. Much like OpenXR SDL is going to require certain Vulkan instance extensions to be enabled in order for the Vulkan surface to be created. We can query these like so:

```cpp
set<string> instanceExtensions;

uint32_t sdlExtensionNameCount;
bool ok = SDL_Vulkan_GetInstanceExtensions(window, &sdlExtensionNameCount, nullptr);

if (!ok)
{
    cerr << "Failed to get SDL Vulkan instance extensions: " << SDL_GetError() << endl;
    return 1;
}

vector<const char*> sdlExtensionNames(sdlExtensionNameCount);
ok = SDL_Vulkan_GetInstanceExtensions(window, &sdlExtensionNameCount, sdlExtensionNames.data());

if (!ok)
{
    cerr << "Failed to get SDL Vulkan instance extensions: " << SDL_GetError() << endl;
    return 1;
}

for (const char* name : sdlExtensionNames)
{
    instanceExtensions.insert(name);
}
```

The only new function here is `SDL_Vulkan_GetInstanceExtensions`, which outputs the list of relevant extensions. Usually these are extensions like `VK_KHR_surface` and `VK_KHR_win32_surface` which enable both the general window surface type and the platform-specific window surface creation function. This then needs to be combined with the list of extensions already requested by OpenXR before the `VkInstance` is created.

Once the instance has been created we can then create the Vulkan surface. We need to do this before initializing the device for reasons that will become clear in a moment. This is done like so:

```cpp
VkSurfaceKHR surface;
ok = SDL_Vulkan_CreateSurface(window, vulkanInstance, &surface);
if (!ok)
{
    cerr << "Failed to create surface: " << SDL_GetError() << endl;
    return 1;
}
```

The purpose of this is to abstract away the many different platform-specific window surface creation functions. Each platform has its own windowing system: Linux has Xlib, XCB and Wayland, Windows has itself, Android has itself and so on. Each one of these platforms has a special process that needs to be performed to create a Vulkan surface from their window handles and this is handled by a number of platform-specific functions like `vkCreateWin32SurfaceKHR`. There are many of these functions and they all need to be called in slightly different ways. This can be quite difficult to manage so SDL wraps them all up in this `SDL_Vulkan_CreateSurface` function which will call the correct one no matter what platform you find yourself compiling for.

Next we need to add an extra device extension, `VK_KHR_swapchain`. This is the standard extension for working with normal non-OpenXR swapchains in Vulkan:

```cpp
deviceExtensions.insert("VK_KHR_swapchain");
```

There's one more change we need to make to device initialization and that's changing the function that finds the relevant device queue family:

```cpp
int32_t getDeviceQueueFamily(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface)
{
    int32_t graphicsQueueFamilyIndex = -1;

    uint32_t queueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

    vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

    for (int32_t i = 0; i < queueFamilyCount; i++)
    {
        VkBool32 present = false;

        VkResult result = vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &present);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to get Vulkan physical device surface support: " << result << endl;
            return graphicsQueueFamilyIndex;
        }

        if (present && (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))
        {
            graphicsQueueFamilyIndex = i;
            break;
        }
    }

    if (graphicsQueueFamilyIndex == -1)
    {
        cerr << "No graphics queue found." << endl;
        return graphicsQueueFamilyIndex;
    }

    return graphicsQueueFamilyIndex;
}
```

Here we've added a call to `vkGetPhysicalDeviceSurfaceSupportKHR` where we pass in the surface we created earlier (this is why it's important the surface is created before the device). This checks if the queue family under consideration is capable of presenting images to that surface. This didn't matter before because OpenXR doesn't use the normal Vulkan presentation system but now we might need to select a different queue because of it.

Now that we have a device we can initialize the Vulkan swapchain. That looks like this:

```cpp
VulkanSwapchain* createVulkanSwapchain(
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkSurfaceKHR surface,
    unsigned width,
    unsigned height
)
{
    VkSwapchainKHR swapchain;

    VkSurfaceCapabilitiesKHR capabilities;
    VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &capabilities);

    uint32_t formatCount;
    result = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr);

    vector<VkSurfaceFormatKHR> formats(formatCount);
    result = vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, formats.data());

    VkSurfaceFormatKHR chosenFormat = formats.front();

    for (const VkSurfaceFormatKHR& format : formats)
    {
        if (format.format == VK_FORMAT_R8G8B8A8_SRGB)
        {
            chosenFormat = format;
            break;
        }
    }

    VkSwapchainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface;
    createInfo.minImageCount = capabilities.minImageCount;
    createInfo.imageFormat = chosenFormat.format;
    createInfo.imageColorSpace = chosenFormat.colorSpace;
    createInfo.imageExtent = { width, height };
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.preTransform = capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    createInfo.clipped = true;

    result = vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan swapchain: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return new VulkanSwapchain(
        device,
        swapchain,
        chosenFormat.format,
        width,
        height
    );
}
```

This should be broadly familiar to anyone who's used Vulkan before. The only things to note are that I've set the `VkSwapchainCreateInfoKHR::imageUsage` member to `VK_IMAGE_USAGE_TRANSFER_DST_BIT` because we're not going to be rendering anything to this swapchain, only copying data to it from the other one. Also note the `VulkanSwapchain` type instantiated at the end. This is just to store the swapchain handle alongside some of its properties for easy access.

With the swapchain created we can set up some images to go with it. First we need a function to retrieve the images that Vulkan has created for us.

```cpp
vector<VkImage> getVulkanSwapchainImages(VkDevice device, VkSwapchainKHR swapchain)
{
    uint32_t imageCount;
    VkResult result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to get Vulkan swapchain images: " << result << endl;
        return {};
    }

    vector<VkImage> images(imageCount);
    result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, images.data());

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to get Vulkan swapchain images: " << result << endl;
        return {};
    }

    return images;
}
```

Again this is very standard Vulkan stuff, no surprises here. Next we create a new `VulkanSwapchainImage` structure to wrap up these raw images, the constructor for which looks like this:

```cpp
VkCommandBufferAllocateInfo allocInfo{};
allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
allocInfo.commandPool = commandPool;
allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
allocInfo.commandBufferCount = 1;

VkResult result = vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

if (result != VK_SUCCESS)
{
    cerr << "Failed to allocate Vulkan command buffers: " << result << endl;
}

VkSemaphoreCreateInfo semaphoreCreateInfo{};
semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

result = vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &renderDoneSemaphore);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan semaphore: " << result << endl;
}
```

We create one command buffer into which we will record the transfer commands that move data from the OpenXR swapchain into this swapchain. We also create one semaphore which will be used to signal the end of the transfer commands so that presentation can begin.

Next we need to make a standalone fence. We'll use this later in the window frame loop:

```cpp
VkFence createFence(VkDevice device)
{
    VkFenceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence fence;

    VkResult result = vkCreateFence(device, &createInfo, nullptr, &fence);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create fence: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return fence;
}
```

It's almost time to start looking at our modified main loop but first we need to modify our OpenXR swapchain and rendering a little. The problem is that we need to get the image data from the OpenXR swapchain but it's difficult to predict what state the OpenXR swapchain will be in when we go to retrieve it. In theory we might leave our swapchain images in `VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL` but by the time we go to read them we might find that SteamVR has converted them to `VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL` and we get validation errors (this is what happened in practice when I tried it). Instead what we're going to do is add a second intermediary image to each swapchain image structure. We're going to copy the data into that image during our render process when we know the image layout for certain, then read from the intermediary image when displaying to our window. Let's get started:

```cpp
VkImageCreateInfo imageCreateInfo{};
imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
imageCreateInfo.extent = { swapchain->width, swapchain->height, 1 };
imageCreateInfo.mipLevels = 1;
imageCreateInfo.arrayLayers = 1;
imageCreateInfo.format = swapchain->format;
imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
imageCreateInfo.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

VmaAllocationCreateInfo imageAllocateInfo{};
imageAllocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

result = vmaCreateImage(
    allocator,
    &imageCreateInfo,
    &imageAllocateInfo,
    &transferImage.image,
    &transferImage.allocation,
    nullptr
);

if (result != VK_SUCCESS)
{
    cerr << "Failed to create Vulkan image: " << result << endl;
}
```

We add this to the constructor for `SwapchainImage` (note: not `VulkanSwapchainImage`), to create a second image of the same size and format as our primary image. This image will be used for transfer reads and writes only so we set `VKImageCreateInfo::usageFlags` to reflect that.

Then, immediately after calling `vkCmdEndRenderPass` in our `renderEyes` function, we do the following:

```cpp
VkImageMemoryBarrier beforeSrcBarrier{};
beforeSrcBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
beforeSrcBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
beforeSrcBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
beforeSrcBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
beforeSrcBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
beforeSrcBarrier.image = image->image.image;
beforeSrcBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
beforeSrcBarrier.subresourceRange.baseMipLevel = 0;
beforeSrcBarrier.subresourceRange.levelCount = 1;
beforeSrcBarrier.subresourceRange.baseArrayLayer = 0;
beforeSrcBarrier.subresourceRange.layerCount = 1;

VkImageMemoryBarrier beforeDstBarrier{};
beforeDstBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
beforeDstBarrier.srcAccessMask = VK_ACCESS_NONE;
beforeDstBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
beforeDstBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
beforeDstBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
beforeDstBarrier.image = image->transferImage.image;
beforeDstBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
beforeDstBarrier.subresourceRange.baseMipLevel = 0;
beforeDstBarrier.subresourceRange.levelCount = 1;
beforeDstBarrier.subresourceRange.baseArrayLayer = 0;
beforeDstBarrier.subresourceRange.layerCount = 1;

VkImageMemoryBarrier beforeBarriers[] = {
    beforeSrcBarrier,
    beforeDstBarrier
};

vkCmdPipelineBarrier(
    image->commandBuffer,
    VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    VK_PIPELINE_STAGE_TRANSFER_BIT,
    0,
    0,
    nullptr,
    0,
    nullptr,
    sizeof(beforeBarriers) / sizeof(VkImageMemoryBarrier),
    beforeBarriers
);

VkImageCopy region{};
region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
region.srcSubresource.baseArrayLayer = 0;
region.srcSubresource.mipLevel = 0;
region.srcSubresource.layerCount = 1;
region.srcOffset = { 0, 0, 0 };
region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
region.dstSubresource.baseArrayLayer = 0;
region.dstSubresource.mipLevel = 0;
region.dstSubresource.layerCount = 1;
region.dstOffset = { 0, 0, 0 };
region.extent = { swapchain->width, swapchain->height, 1 };

vkCmdCopyImage(
    image->commandBuffer,
    image->image.image,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    image->transferImage.image,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    1,
    &region
);

VkImageMemoryBarrier afterSrcBarrier{};
afterSrcBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
afterSrcBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
afterSrcBarrier.dstAccessMask = VK_ACCESS_NONE;
afterSrcBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
afterSrcBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
afterSrcBarrier.image = image->image.image;
afterSrcBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
afterSrcBarrier.subresourceRange.baseMipLevel = 0;
afterSrcBarrier.subresourceRange.levelCount = 1;
afterSrcBarrier.subresourceRange.baseArrayLayer = 0;
afterSrcBarrier.subresourceRange.layerCount = 1;

VkImageMemoryBarrier afterDstBarrier{};
afterDstBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
afterDstBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
afterDstBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
afterDstBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
afterDstBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
afterDstBarrier.image = image->transferImage.image;
afterDstBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
afterDstBarrier.subresourceRange.baseMipLevel = 0;
afterDstBarrier.subresourceRange.levelCount = 1;
afterDstBarrier.subresourceRange.baseArrayLayer = 0;
afterDstBarrier.subresourceRange.layerCount = 1;

VkImageMemoryBarrier afterBarriers[] = {
    afterSrcBarrier,
    afterDstBarrier
};

vkCmdPipelineBarrier(
    image->commandBuffer,
    VK_PIPELINE_STAGE_TRANSFER_BIT,
    VK_PIPELINE_STAGE_TRANSFER_BIT,
    0,
    0,
    nullptr,
    0,
    nullptr,
    sizeof(afterBarriers) / sizeof(VkImageMemoryBarrier),
    afterBarriers
);
```

This is very lengthy but it's just a standard Vulkan image-to-image copy. We transition the swapchain image into the transfer source layout, the intermediary image into the transfer destination layout, copy and then return the swapchain image to `VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL` and the intermediary image to `VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL` so it's ready for reading into the other swapchain.

There's one more change we need to make to the render process and that's to return the `activeIndex` variable back to the `main` function. This is a variable that's filled by `xrAcquireSwapchainImage` inside of `renderEyes` and indicates which element of the OpenXR swapchain will be rendered into. This is important because when copying to our other swapchain later we need to know which image was rendered most recently so we can display the most recent possible frame. I won't show the code for this here because it's scattered all over the place, but it looks exactly how you'd expect.

At this point we have a Vulkan swapchain fully initialized and the data it needs to be useful is ready to go in the intermediary images attached to our OpenXR swapchain. All that's left is a main loop that puts these parts together. We start by adding two variables before our main loop:

```cpp
uint32_t imageIndex = 0;
uint32_t lastRenderedImageIndex = 0;
```

One of these will store the active Vulkan swapchain index, while the other will keep track of the last value of `activeIndex` for the OpenXR swapchain. At the start of our main loop we add some event handling boilerplate:

```cpp
SDL_Event event;

while (SDL_PollEvent(&event))
{
    switch (event.type)
    {
    case SDL_QUIT :
        quit = true;
        break;
    case SDL_WINDOWEVENT :
        switch (event.window.event)
        {
        case SDL_WINDOWEVENT_SIZE_CHANGED :
        {
            recreateSwapchain(
                surface,
                physicalDevice,
                device,
                commandPool,
                &vulkanSwapchain,
                &wrappedVulkanSwapchainImages,
                &imageIndex,
                event.window.data1,
                event.window.data2
            );
            break;
        }
        }
        break;
    }
}
```

Here we handle just two of SDL's many many events, `SDL_QUIT` and `SDL_WINDOEVENT_SIZE_CHANGED`. `SDL_QUIT` is emitted when the user closes the spectator view window and we take this as a signal to shut down the application. `SDL_WINDOWEVENT_SIZE_CHANGED` is, predictably, encountered when the window changes size for any reason. When this happens we need to recreate the swapchain and swapchain images. We define a function to do this because several different places in our code can trigger a resize. That function is defined like so:

```cpp
void recreateSwapchain(
    VkSurfaceKHR surface,
    VkPhysicalDevice physicalDevice,
    VkDevice device,
    VkCommandPool commandPool,
    VulkanSwapchain** vulkanSwapchain,
    vector<VulkanSwapchainImage*>* wrappedVulkanSwapchainImages,
    uint32_t* imageIndex,
    unsigned width,
    unsigned height
)
{
    VkResult result = vkDeviceWaitIdle(device);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to wait for Vulkan device to idle: " << result << endl;
    }

    for (VulkanSwapchainImage* image : *wrappedVulkanSwapchainImages)
    {
        delete image;
    }
    wrappedVulkanSwapchainImages->clear();

    delete *vulkanSwapchain;

    *vulkanSwapchain = createVulkanSwapchain(
        physicalDevice,
        device,
        surface,
        width,
        height
    );

    vector<VkImage> images = getVulkanSwapchainImages(device, (*vulkanSwapchain)->swapchain);

    *wrappedVulkanSwapchainImages = vector<VulkanSwapchainImage*>(images.size());
    for (size_t i = 0; i < wrappedVulkanSwapchainImages->size(); i++)
    {
        wrappedVulkanSwapchainImages->at(i) = new VulkanSwapchainImage(device, commandPool, images.at(i));
    }

    *imageIndex = 0;
}
```

Again this is all very standard Vulkan code: bring the GPU to a halt, delete the current swapchain and create a new one. Bringing the GPU to a halt like this will necessarily cause a hitch from the perspective of the user inside the headset but only when the window on the desktop is resized which is infrequently enough it should be tolerable. It's possible this could be avoided entirely by carefully waiting on semaphores or fences to track the completion of window-specific GPU tasks but that's beyond the scope of this tutorial.

Once the event loop is dealt with we can do the actual rendering code for the spectator view window. We start by trying to acquire an image like so:

```cpp
VkResult vkResult = vkAcquireNextImageKHR(
    device,
    vulkanSwapchain->swapchain,
    0,
    nullptr,
    acquireFence,
    &imageIndex
);

if (vkResult == VK_ERROR_OUT_OF_DATE_KHR || vkResult == VK_SUBOPTIMAL_KHR)
{
    int width;
    int height;
    SDL_GetWindowSize(window, &width, &height);

    recreateSwapchain(
        surface,
        physicalDevice,
        device,
        commandPool,
        &vulkanSwapchain,
        &wrappedVulkanSwapchainImages,
        &imageIndex,
        width,
        height
    );
}
```

Here we're using the `acquireFence` we created earlier because `vkAcquireNextImageKHR` requires a fence and/or semaphore as one of its arguments. We set the timeout to zero because we absolutely do not want to slow down the VR render loop by waiting for this secondary less important view to be ready to receive frames, especially because it's very likely that our VR output will be targeting 90 or 120 Hz while the spectator view is capped at 60. If `vkAcquireNextImageKHR` successfully acquires an image it will return `VK_SUCCESS`. When we get that return code we then wait on and subsequently reset the fence, enabling it to be used again next frame:

```cpp
vkResult = vkWaitForFences(device, 1, &acquireFence, true, numeric_limits<uint64_t>::max());

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to wait for Vulkan fence: " << vkResult << endl;
}

vkResult = vkResetFences(device, 1, &acquireFence);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to reset Vulkan fence: " << vkResult << endl;
}
```

Next we record some commands:

```
VulkanSwapchainImage* element = wrappedVulkanSwapchainImages.at(imageIndex);

VkCommandBufferBeginInfo beginInfo{};
beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

vkResult = vkBeginCommandBuffer(element->commandBuffer, &beginInfo);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to begin Vulkan command buffer: " << vkResult << endl;
}

VkImageMemoryBarrier beforeDstBarrier{};
beforeDstBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
beforeDstBarrier.srcAccessMask = VK_ACCESS_NONE;
beforeDstBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
beforeDstBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
beforeDstBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
beforeDstBarrier.image = element->image;
beforeDstBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
beforeDstBarrier.subresourceRange.baseMipLevel = 0;
beforeDstBarrier.subresourceRange.levelCount = 1;
beforeDstBarrier.subresourceRange.baseArrayLayer = 0;
beforeDstBarrier.subresourceRange.layerCount = 1;

VkImageMemoryBarrier beforeBarriers[] = {
    beforeDstBarrier
};

vkCmdPipelineBarrier(
    element->commandBuffer,
    VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
    VK_PIPELINE_STAGE_TRANSFER_BIT,
    0,
    0,
    nullptr,
    0,
    nullptr,
    sizeof(beforeBarriers) / sizeof(VkImageMemoryBarrier),
    beforeBarriers
);

VkImageBlit region{};
region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
region.srcSubresource.mipLevel = 0;
region.srcSubresource.baseArrayLayer = 0;
region.srcSubresource.layerCount = 1;
region.srcOffsets[0] = { 0, 0, 0 };
region.srcOffsets[1] = { (int32_t)swapchain->width, (int32_t)swapchain->height, 1 };
region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
region.dstSubresource.mipLevel = 0;
region.dstSubresource.baseArrayLayer = 0;
region.dstSubresource.layerCount = 1;
region.dstOffsets[0] = { 0, 0, 0 };
region.dstOffsets[1] = { (int32_t)vulkanSwapchain->width, (int32_t)vulkanSwapchain->height, 1 };

vkCmdBlitImage(
    element->commandBuffer,
    wrappedSwapchainImages[lastRenderedImageIndex]->transferImage.image,
    VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
    element->image,
    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
    1,
    &region,
    VK_FILTER_LINEAR
);

VkImageMemoryBarrier afterDstBarrier{};
afterDstBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
afterDstBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
afterDstBarrier.dstAccessMask = VK_ACCESS_NONE;
afterDstBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
afterDstBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
afterDstBarrier.image = element->image;
afterDstBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
afterDstBarrier.subresourceRange.baseMipLevel = 0;
afterDstBarrier.subresourceRange.levelCount = 1;
afterDstBarrier.subresourceRange.baseArrayLayer = 0;
afterDstBarrier.subresourceRange.layerCount = 1;

VkImageMemoryBarrier afterBarriers[] = {
    afterDstBarrier
};

vkCmdPipelineBarrier(
    element->commandBuffer,
    VK_PIPELINE_STAGE_TRANSFER_BIT,
    VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
    0,
    0,
    nullptr,
    0,
    nullptr,
    sizeof(afterBarriers) / sizeof(VkImageMemoryBarrier),
    afterBarriers
);

vkResult = vkEndCommandBuffer(element->commandBuffer);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to end Vulkan command buffer: " << vkResult << endl;
}
```

These are very similar to the copy command from before. They just put the swapchain image into the transfer destination layout, blit the intermediary image into it and then transition the swapchain image into the presentation layout ready for display. I'm using a blit here because it's very unlikely that our window and VR view will have even vaguely similar resolutions and unlike `vkCmdCopyImage` `vkCmdBlitImage` is capable of dynamic resizing with linear filtering.

Next we submit our commands to the GPU:

```cpp
VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

VkSubmitInfo submitInfo{};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.pWaitDstStageMask = &waitStage;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &element->commandBuffer;
submitInfo.signalSemaphoreCount = 1;
submitInfo.pSignalSemaphores = &element->renderDoneSemaphore;

vkResult = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

if (vkResult != VK_SUCCESS)
{
    cerr << "Failed to submit Vulkan queue: " << vkResult << endl;
}
```

This is again pretty standard for Vulkan, the only unusual detail is we're waiting for the transfer stage to end rather than the color attachment output stage, because we're not doing any rendering at all here, only a transfer. Finally we submit the present command:

```cpp
VkPresentInfoKHR presentInfo{};
presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
presentInfo.waitSemaphoreCount = 1;
presentInfo.pWaitSemaphores = &element->renderDoneSemaphore;
presentInfo.swapchainCount = 1;
presentInfo.pSwapchains = &vulkanSwapchain->swapchain;
presentInfo.pImageIndices = &imageIndex;

vkResult = vkQueuePresentKHR(queue, &presentInfo);

if (vkResult == VK_ERROR_OUT_OF_DATE_KHR || vkResult == VK_SUBOPTIMAL_KHR)
{
    int width;
    int height;
    SDL_GetWindowSize(window, &width, &height);

    recreateSwapchain(
        surface,
        physicalDevice,
        device,
        commandPool,
        &vulkanSwapchain,
        &wrappedVulkanSwapchainImages,
        &imageIndex,
        width,
        height
    );
}
else if (vkResult < 0)
{
    cerr << "Failed to present Vulkan queue: " << vkResult << endl;
}
```

This is tied to the `vkQueueSubmit` command through the semaphore which ensures that it won't execute until our blit has completed. And that's it! You should now have an OpenXR application with a spectator window on the desktop. I've covered everything of importance here but to get a complete overview of the changes involved in adding this functionality to your existing OpenXR application I recommend comparing the code examples from addendum 2 and addendum 3 in your favorite diff tool, which will highlight all the relevant sections.

There's lots more that could be done here. Right now I'm taking the roughly square image from the OpenXR swapchain and blitting it into a probably landscape aspect-ratio window on the desktop, resulting quite a lot of stretching and distortion. You might want to approach this more cleverly and clear the window with black color, then blit into a square region in the center of the window with black bars at the sides. I'm also only copying the first array layer of the OpenXR swapchain image, which is the left eye view. This will mean that spectators will see a view that is noticeably off-center from the center of the player's head and sometimes the player will be interacting with things that are out of shot, far to the right. You could display both eye views side-by-side in the spectator window by copying both array layers, or you could render to a third camera placed between the player's eyes to get the ideal spectator experience at the cost of some performance.

Finally a comment on the Vulkan frame loop. Some particularly observant readers may notice it differs quite a bit from the normal sequence of semaphore and fence operations found in other Vulkan projects, for example `VkSubmitInfo::pSignalSemaphores` goes unused. I'll be blunt: I don't understand why this is. I tried the classic frame loop originally and while <i>it</i> seemed to work fine, submitting those commands to the same queue being used by OpenXR caused OpenXR to freeze up on the `xrEndFrame`. This happened across both Monado and SteamVR so I don't believe it's anything to do with the runtime, it must be some Vulkan control flow interaction I don't fully understand. If anyone better understands this please let me know so I can update this tutorial with more information.
