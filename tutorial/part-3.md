This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 3: OpenXR and Vulkan integration

Now that we have an OpenXR system ID we have to create a "session". A session is a context within which we access inputs and render frames. But that "render frames" part is a problem. In order to accept frames from our application, the OpenXR session needs to know what our graphics environment looks like. But OpenXR also gets a say in what our graphics environment looks like. The OpenXR runtime might use our graphics environment to implement some of its own functionality, such as system overlays, and so it might want us to support specific features. Additionally, if you have multiple graphics devices in your system it would probably be best if we used the one the XR headset is attached to, so OpenXR can help us find that out too.

We're going to add two new functions to do handle this, the first of which is this:

```
tuple<XrGraphicsRequirementsVulkanKHR, set<string>> getVulkanInstanceRequirements(XrInstance instance, XrSystemId system)
{
    auto xrGetVulkanGraphicsRequirementsKHR = (PFN_xrGetVulkanGraphicsRequirementsKHR)getXRFunction(instance, "xrGetVulkanGraphicsRequirementsKHR");
    auto xrGetVulkanInstanceExtensionsKHR = (PFN_xrGetVulkanInstanceExtensionsKHR)getXRFunction(instance, "xrGetVulkanInstanceExtensionsKHR");

    XrGraphicsRequirementsVulkanKHR graphicsRequirements{};
    graphicsRequirements.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR;

    XrResult result = xrGetVulkanGraphicsRequirementsKHR(instance, system, &graphicsRequirements);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan graphics requirements: " << result << endl;
        return { graphicsRequirements, {} };
    }

    uint32_t instanceExtensionsSize;

    result = xrGetVulkanInstanceExtensionsKHR(instance, system, 0, &instanceExtensionsSize, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan instance extensions: " << result << endl;
        return { graphicsRequirements, {} };
    }

    char* instanceExtensionsData = new char[instanceExtensionsSize];

    result = xrGetVulkanInstanceExtensionsKHR(instance, system, instanceExtensionsSize, &instanceExtensionsSize, instanceExtensionsData);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan instance extensions: " << result << endl;
        return { graphicsRequirements, {} };
    }

    set<string> instanceExtensions;

    uint32_t last = 0;
    for (uint32_t i = 0; i <= instanceExtensionsSize; i++)
    {
        if (i == instanceExtensionsSize || instanceExtensionsData[i] == ' ')
        {
            instanceExtensions.insert(string(instanceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] instanceExtensionsData;

    return { graphicsRequirements, instanceExtensions };
}
```

This function does two things. First it uses `xrGetVulkanGraphicsRequirementsKHR` to fill an `XrGraphicsRequirementsVulkanKHR` structure. This structure contains minimum and maximum supported version numbers incidating which versions of Vulkan the OpenXR session will accept. We'll need to feed these to Vulkan later. The second thing it does is use `xrGetVulkanInstanceExtensionsKHR` to get a list of all the Vulkan instance extensions the OpenXR session will expect us to have enabled. The first call to this function gets the number of characters in the string listing all the instance extension names. The second call gets the actual string so we can inspect it. After retrieving the string we have a bit of code to convert it from one long space-separated string into a set of discrete names. The second function we're going to add is this:

```
tuple<VkPhysicalDevice, set<string>> getVulkanDeviceRequirements(XrInstance instance, XrSystemId system, VkInstance vulkanInstance)
{
    auto xrGetVulkanGraphicsDeviceKHR = (PFN_xrGetVulkanGraphicsDeviceKHR)getXRFunction(instance, "xrGetVulkanGraphicsDeviceKHR");
    auto xrGetVulkanDeviceExtensionsKHR = (PFN_xrGetVulkanDeviceExtensionsKHR)getXRFunction(instance, "xrGetVulkanDeviceExtensionsKHR");

    VkPhysicalDevice physicalDevice;

    XrResult result = xrGetVulkanGraphicsDeviceKHR(instance, system, vulkanInstance, &physicalDevice);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan graphics device: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    uint32_t deviceExtensionsSize;

    result = xrGetVulkanDeviceExtensionsKHR(instance, system, 0, &deviceExtensionsSize, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan device extensions: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    char* deviceExtensionsData = new char[deviceExtensionsSize];

    result = xrGetVulkanDeviceExtensionsKHR(instance, system, deviceExtensionsSize, &deviceExtensionsSize, deviceExtensionsData);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan device extensions: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    set<string> deviceExtensions;

    uint32_t last = 0;
    for (uint32_t i = 0; i <= deviceExtensionsSize; i++)
    {
        if (i == deviceExtensionsSize || deviceExtensionsData[i] == ' ')
        {
            deviceExtensions.insert(string(deviceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] deviceExtensionsData;

    return { physicalDevice, deviceExtensions };
}
```

Like the previous function, this function does two things. First it uses `xrGetVulkanGraphicsDeviceKHR` to identify which physical graphics device in the system should be used. This can be important for example in laptops with both integrated and discrete graphics, or in desktops with multiple graphics cards, only one of which has the XR headset attached. Secondly it uses `xrGetVulkanDeviceExtensionsKHR` to retrieve the device extension names the OpenXR session will expect us to have enabled. This process is exactly the same as in the previous function, but needs to be repeated because Vulkan has two different sets of extensions: instance and device.

Finally we can add a call to the first function to our main:

```
int main(int, char**)
{
    XrInstance instance = createInstance();
    XrDebugUtilsMessengerEXT debugMessenger = createDebugMessenger(instance);
    XrSystemId system = getSystem(instance);

    XrGraphicsRequirementsVulkanKHR graphicsRequirements;
    set<string> instanceExtensions;
    tie(graphicsRequirements, instanceExtensions) = getVulkanInstanceRequirements(instance, system);

    destroyDebugMessenger(instance, debugMessenger);
    destroyInstance(instance);

    return 0;
}
```

We'll leave the second function out for now as it can't be called until after the Vulkan instance has been initialized, which we'll be doing next time. And that's it, we're now ready for the next part, where we'll actually initialize the entire Vulkan environment.
