This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 2: Getting the system

Next we need to get the "system". This represents a specific hardware platform connected to the instance. Getting a system is very easy:

```
XrSystemId getSystem(XrInstance instance)
{
    XrSystemId systemID;

    XrSystemGetInfo systemGetInfo{};
    systemGetInfo.type = XR_TYPE_SYSTEM_GET_INFO;
    systemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

    XrResult result = xrGetSystem(instance, &systemGetInfo, &systemID);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get system: " << result << endl;
        return XR_NULL_SYSTEM_ID;
    }

    return systemID;
}
```

The only option here is `formFactor`, which can be either `XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY` or `XR_FORM_FACTOR_HANDHELD_DISPLAY`. We can just add this to our `main`:

```
int main(int, char**)
{
    XrInstance = createInstance();
    XrDebugUtilsMessengerEXT debugMessenger = createDebugMessenger(instance);
    XrSystemId systemID = getSystem(instance);

    destroyDebugMessnger(instance, debugMessenger);
    destroyInstance(instance);

    return 0;
}
```

We haven't "created" anything here so there's no need to add a matching cleanup function for this.
