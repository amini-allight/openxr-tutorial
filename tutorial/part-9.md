This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 9: Input

For the final part of this tutorial, we'll be discussing the OpenXR input system. Compared to all that work we did to get video output working, input from the controllers is comparatively easy to handle. The first thing we'll need for handling input is an action set. OpenXR's input system is based on the idea of "actions". These are not physical inputs, but logical commands within your application i.e. "move hand", "grip object", "eject magazine" or "increase throttle". These actions are grouped into sets which can represent different states of your application i.e. "menu", "gameplay" or "driving vehicle". Usually at least one action set is "attached" to the session at any one time, sometimes multiple. In this example we'll just create one:

```
XrActionSet createActionSet(XrInstance instance)
{
    XrActionSet actionSet;

    XrActionSetCreateInfo actionSetCreateInfo{};
    actionSetCreateInfo.type = XR_TYPE_ACTION_SET_CREATE_INFO;
    strcpy(actionSetCreateInfo.actionSetName, "openxr_example");
    strcpy(actionSetCreateInfo.localizedActionSetName, applicationName);
    actionSetCreateInfo.priority = 0;

    XrResult result = xrCreateActionSet(instance, &actionSetCreateInfo, &actionSet);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create action set: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return actionSet;
}
```

Action sets need to have two names, an internal name and a localized name. The internal name is (I believe) used by the runtime for managing things like storing and retrieving user input rebindings. The internal name must be a well-formed single-level path string, which I believe means it must contain only alphanumeric characters and underscores, as the path strings provided by OpenXR itself do. The localized name is shown to the user in the user interface of their runtime when viewing/adjusting input bindings and should be adjusted to their platform language. This is just an example program so we'll just set both of these to our application name, using a custom version for the internal name to meet the format requirements. Be aware that as with our earlier encounters with fixed-length string fields, there are hard length limits here which need to be observed. Action sets also have a priority level, with action sets with a larger priority level taking precedence over ones with a lower value in situations where multiple attached action sets listen to the same physical input. As usual we need to clean this object up:

```
void destroyActionSet(XrActionSet actionSet)
{
    xrDestroyActionSet(actionSet);
}
```

Now we need some actions to go in our action set. We'll be making a few of these, so we need a generic creation function that can be customized:

```
XrAction createAction(XrActionSet actionSet, const char* name, XrActionType type)
{
    XrAction action;

    XrActionCreateInfo actionCreateInfo{};
    actionCreateInfo.type = XR_TYPE_ACTION_CREATE_INFO;
    strcpy(actionCreateInfo.actionName, name);
    strcpy(actionCreateInfo.localizedActionName, name);
    actionCreateInfo.actionType = type;

    XrResult result = xrCreateAction(actionSet, &actionCreateInfo, &action);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create action: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return action;
}
```

As with action sets, actions have both an internal name and a user-facing localized name. They also have an action type, which can be one of a number of values like `XR_ACTION_TYPE_BOOLEAN_INPUT` for binary button inputs or `XR_ACTION_TYPE_FLOAT_INPUT` for inputs with a range of values like triggers. They also need to be cleaned up:

```
void destroyAction(XrAction action)
{
    xrDestroyAction(action);
}
```

Actions which use `XR_ACTION_TYPE_POSE_INPUT` also require an "action space" which represents a space relative to the controller (or other form of pose input). 

```
XrSpace createActionSpace(XrSession session, XrAction action)
{
    XrSpace space;

    XrActionSpaceCreateInfo actionSpaceCreateInfo{};
    actionSpaceCreateInfo.type = XR_TYPE_ACTION_SPACE_CREATE_INFO;
    actionSpaceCreateInfo.poseInActionSpace.position = { 0, 0, 0 };
    actionSpaceCreateInfo.poseInActionSpace.orientation = { 0, 0, 0, 1 };
    actionSpaceCreateInfo.action = action;

    XrResult result = xrCreateActionSpace(session, &actionSpaceCreateInfo, &space);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create action space: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return space;
}
```

`poseInActionSpace` can be used to apply an offset relative to the basic input: for example if you want an action space that sits above and in front of a controller, lining up with where a weapon in your application emits projectiles. And again we have to clean up:

```
void destroyActionSpace(XrSpace actionSpace)
{
    xrDestroySpace(actionSpace);
}
```

Now we'll use these functions in our `main` to set up some objects:

```
XrActionSet actionSet = createActionSet(instance);

XrAction leftHandAction = createAction(actionSet, "left-hand", XR_ACTION_TYPE_POSE_INPUT);
XrAction rightHandAction = createAction(actionSet, "right-hand", XR_ACTION_TYPE_POSE_INPUT);
XrAction leftGrabAction = createAction(actionSet, "left-grab", XR_ACTION_TYPE_BOOLEAN_INPUT);
XrAction rightGrabAction = createAction(actionSet, "right-grab", XR_ACTION_TYPE_BOOLEAN_INPUT);

XrSpace leftHandSpace = createActionSpace(session, leftHandAction);
XrSpace rightHandSpace = createActionSpace(session, rightHandAction);

destroyActionSpace(rightHandSpace);
destroyActionSpace(leftHandSpace);

destroyAction(rightGrabAction);
destroyAction(leftGrabAction);
destroyAction(rightHandAction);
destroyAction(leftHandAction);

destroyActionSet(actionSet);
```

Next we need to suggest some physical inputs to bind to these actions. These are, as the name implies, suggestions. The runtime is free to ignore them and users are free to manually override them in the runtime's settings, but in practice they will be observed by default and rarely overriden. To start with we're going to be getting some paths, which are internal OpenXR symbols which represent various physical inputs and devices. We'll write a helper function to get these from their string names:

```
XrPath getPath(XrInstance instance, const char* name)
{
    XrPath path;

    XrResult result = xrStringToPath(instance, name, &path);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get path '" << name << "': " << result << endl;
        return XR_NULL_PATH;
    }

    return path;
}
```

And then we'll use this helper functions to get some paths and suggest our actions be bound to them:

```
void suggestBindings(
    XrInstance instance,
    XrAction leftHandAction,
    XrAction rightHandAction,
    XrAction leftGrabAction,
    XrAction rightGrabAction
)
{
    XrPath leftHandPath = getPath(instance, "/user/hand/left/input/grip/pose");
    XrPath rightHandPath = getPath(instance, "/user/hand/right/input/grip/pose");
    XrPath leftButtonPath = getPath(instance, "/user/hand/left/input/a/click");
    XrPath rightButtonPath = getPath(instance, "/user/hand/right/input/a/click");
    XrPath interactionProfilePath = getPath(instance, "/interaction_profiles/valve/index_controller");

    XrActionSuggestedBinding suggestedBindings[] = {
        { leftHandAction, leftHandPath },
        { rightHandAction, rightHandPath },
        { leftGrabAction, leftButtonPath },
        { rightGrabAction, rightButtonPath }
    };

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = interactionProfilePath;
    suggestedBinding.countSuggestedBindings = sizeof(suggestedBindings) / sizeof(XrActionSuggestedBinding);
    suggestedBinding.suggestedBindings = suggestedBindings;

    XrResult result = xrSuggestInteractionProfileBindings(instance, &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to suggest interaction profile bindings: " << result << endl;
    }
}
```

As you can probably tell from the use of `/interaction_profiles/valve/index_controller` we're only suggesting bindings for the Valve Index controllers, because these are the controllers I happen to use. In a real application you'll likely want to suggest bindings for a wide variety of different hardware devices by calling `xrSuggestInteractionProfileBindings` multiple times. A list of available input paths can be found [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html#semantic-path-interaction-profiles) and more are available through various OpenXR extensions documented further down the same page. Note that the position of the headset itself is **not** considered to be an input, if you need headset position (for example for character IK in a multiplayer application) you must get it from the `xrLocateViews` function used in the previous tutorial segment.

Now we can attach our action set to the session, making our actions available to be triggered by physical inputs:

```
void attachActionSet(XrSession session, XrActionSet actionSet)
{
    XrSessionActionSetsAttachInfo actionSetsAttachInfo{};
    actionSetsAttachInfo.type = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO;
    actionSetsAttachInfo.countActionSets = 1;
    actionSetsAttachInfo.actionSets = &actionSet;

    XrResult result = xrAttachSessionActionSets(session, &actionSetsAttachInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to attach action set: " << result << endl;
    }
}
```

Now we'll add a function which gets the state of a boolean input:

```
bool getActionBoolean(XrSession session, XrAction action)
{
    XrActionStateGetInfo getInfo{};
    getInfo.type = XR_TYPE_ACTION_STATE_GET_INFO;
    getInfo.action = action;

    XrActionStateBoolean state{};
    state.type = XR_TYPE_ACTION_STATE_BOOLEAN;

    XrResult result = xrGetActionStateBoolean(session, &getInfo, &state);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get boolean action state: " << result << endl;
        return false;
    }

    return state.currentState;
}
```

`XrActionStateBoolean` has a number of fields that may be of interest for certain applications, like the `changedSinceLastSync` flag to detect when the action's state undergoes a transition and `isActive` if the action is in fact connected to a physical input. For our purposes though we're happy with just `currentState`, which contains the actual on/off value of the button. We need another more complex function to get the state of a pose input:

```
XrPosef getActionPose(XrSession session, XrAction action, XrSpace space, XrSpace roomSpace, XrTime predictedDisplayTime)
{
    XrPosef pose = {
        { 0, 0, 0, 1 },
        { 0, 0, 0 }
    };

    XrActionStateGetInfo getInfo{};
    getInfo.type = XR_TYPE_ACTION_STATE_GET_INFO;
    getInfo.action = action;

    XrActionStatePose state{};
    state.type = XR_TYPE_ACTION_STATE_POSE;

    XrResult result = xrGetActionStatePose(session, &getInfo, &state);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get pose action state: " << result << endl;
        return pose;
    }

    XrSpaceLocation location{};
    location.type = XR_TYPE_SPACE_LOCATION;

    result = xrLocateSpace(space, roomSpace, predictedDisplayTime, &location);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to locate space: " << result << endl;
        return pose;
    }

    if (
        !(location.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) ||
        !(location.locationFlags & XR_SPACE_LOCATION_ORIENTATION_TRACKED_BIT)
    )
    {
        cerr << "Received incomplete result when locating space." << endl;
        return pose;
    }

    return location.pose;
}
```

We technically don't use the values retrieved by `xrGetActionStatePose` here but I've included it for completeness, in a real application you may wish to read `XrActionStatePose::isActive`. Now we can start constructing the function that will read values from the actions and use them to manipulate the world. Now we can add a function call to our main loop to read the input:

```
quit |= !input(
    session,
    actionSet,
    space,
    frameState.predictedDisplayTime,
    leftHandAction,
    rightHandAction,
    leftGrabAction,
    rightGrabAction,
    leftHandSpace,
    rightHandSpace
);
```

And then we'll define the function above

```
bool input(
    XrSession session,
    XrActionSet actionSet,
    XrSpace roomSpace,
    XrTime predictedDisplayTime,
    XrAction leftHandAction,
    XrAction rightHandAction,
    XrAction leftGrabAction,
    XrAction rightGrabAction,
    XrSpace leftHandSpace,
    XrSpace rightHandSpace
)
{

}
```

First we need to request OpenXR retrieve the current state of our previously defined and attached actions from the runtime:

```
XrActiveActionSet activeActionSet = {
    actionSet,
    XR_NULL_PATH
};

XrActionsSyncInfo syncInfo{};
syncInfo.type = XR_TYPE_ACTIONS_SYNC_INFO;
syncInfo.countActiveActionSets = 1;
syncInfo.activeActionSets = &activeActionSet;

XrResult result = xrSyncActions(session, &syncInfo);

if (result == XR_SESSION_NOT_FOCUSED)
{
    return true;
}
else if (result != XR_SUCCESS)
{
    cerr << "Failed to synchronize actions: " << result << endl;
    return false;
}
```

There isn't too much to talk about here, except that `XrActiveActionSet` provides `subactionPath` (set to `XR_NULL_PATH` here) which can be used to only sync a specific subset of actions and that `xrSyncActions` has the non-success non-fatal result code `XR_SESSION_NOT_FOCUSED` that we should be aware of. Moving onto the actual game logic, we use the functions we defined earlier to extract information about our inputs:

```
XrPosef leftHand = getActionPose(session, leftHandAction, leftHandSpace, roomSpace, predictedDisplayTime);
XrPosef rightHand = getActionPose(session, rightHandAction, rightHandSpace, roomSpace, predictedDisplayTime);

bool leftGrab = getActionBoolean(session, leftGrabAction);
bool rightGrab = getActionBoolean(session, rightGrabAction);
```

And then use those values to manipulate the position of the object:

```
if (leftGrab && !objectGrabbed && sqrt(pow(objectPos.x - leftHand.position.x, 2) + pow(objectPos.y - leftHand.position.y, 2) + pow(objectPos.z - leftHand.position.z, 2)) < grabDistance)
{
    objectGrabbed = 1;
}
else if (!leftGrab && objectGrabbed == 1)
{
    objectGrabbed = 0;
}

if (rightGrab && !objectGrabbed && sqrt(pow(objectPos.x - leftHand.position.x, 2) + pow(objectPos.y - leftHand.position.y, 2) + pow(objectPos.z - leftHand.position.z, 2)) < grabDistance)
{
    objectGrabbed = 2;
}
else if (!rightGrab && objectGrabbed == 2)
{
    objectGrabbed = 0;
}

switch (objectGrabbed)
{
case 0 :
    break;
case 1 :
    objectPos = leftHand.position;
    break;
case 2 :
    objectPos = rightHand.position;
    break;
}

return true;
```

Then finally we're going to alter the model matrix we defined in part 8 so that it uses the position value we alter in the game logic:

```
float modelMatrix[4][4]{
    { 1, 0, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 1, 0 },
    { objectPos.x, objectPos.y, objectPos.z, 1 }
};
```

And that's it! If you put your headset on you should be able to pick up the triangle by holding A near it, move it around attached to your hand, and drop it somewhere else and with that we've completed our basic example application. There are a few things that haven't been covered, most obviously anti-aliasing, but they should be relatively easy to figure out.
