This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Addendum 2: Single Swapchain

One of the biggest things I've become unhappy with since I wrote the original OpenXR tutorial is the way it uses two swapchains. This is definitely a valid way to use OpenXR but it tends to overcomplicate things. Most obviously you tend up with two swapchains and two sets of swapchain images, but technically it might even result in two copies of every pipeline and render pass, given the two swapchains could theoretically differ in the requested multisampling levels. Worse still, the use of two swapchains decreases performance. This is especially true in the approach I use in the OpenXR tutorial, creating, recording and submitting two entirely separate command buffers, but it would also be true even with a unified command buffer: two separate render targets means two separate render pass invocations and two copies of every draw call, one of the scarcest resources in modern graphics programming.

Today I want to take you through the process of using a single swapchain to reap all the benefits that provides. We'll also be using multiview rendering, where a single draw command is executed for multiple render targets simultaneously through use of slightly modified shaders. This does make the shaders incompatible with flat mode rendering (unless you treat flat mode as multiview-with-only-one-view) but it allows us to cut the number of draw calls and render pass invocations in half.

The example program for this tutorial is a slightly modified version of the one from part 9, in fact if you've already followed the tutorial up to part 9 you might just want to diff the two in your favorite text diffing tool and go through the changes at your own pace. For the sake of completeness though I'll be documenting all the changes made here in the order I think makes most sense.

The first thing we're going to need to use a single swapchain is, well, a single swapchain. To do this we replace the for loop we used to create two swapchains with a single invocation of the `xrCreateSwapchain` command:

```cpp
XrSwapchain swapchain;

XrSwapchainCreateInfo swapchainCreateInfo{};
swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
swapchainCreateInfo.format = chosenFormat;
swapchainCreateInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
swapchainCreateInfo.width = configViews[0].recommendedImageRectWidth;
swapchainCreateInfo.height = configViews[0].recommendedImageRectHeight;
swapchainCreateInfo.faceCount = 1;
swapchainCreateInfo.arraySize = eyeCount;
swapchainCreateInfo.mipCount = 1;

result = xrCreateSwapchain(session, &swapchainCreateInfo, &swapchain);

if (result != XR_SUCCESS)
{
    cerr << "Failed to create swapchain: " << result << endl;
    return nullptr;
}

return new Swapchain(
    swapchain,
    (VkFormat)chosenFormat,
    configViews[0].recommendedImageRectWidth,
    configViews[0].recommendedImageRectHeight
);
```

Aside from reducing the number of calls from 2 to 1 the only other change here is changing `arraySize` from 1 to `eyeCount`. The next thing we need to do is get access to the GPU's multiview rendering feature, which is exposed through the `VK_KHR_multiview` extension. You can either request this as a device extension or you can just bump the version up to 1.1 where it was promoted and became part of the core standard. I've chosen the second approach for this tutorial.

```cpp
VkApplicationInfo applicationInfo{};
applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
applicationInfo.pApplicationName = applicationName;
applicationInfo.applicationVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
applicationInfo.pEngineName = applicationName;
applicationInfo.engineVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
applicationInfo.apiVersion = max(VK_API_VERSION_1_1, VK_MAKE_API_VERSION(
    0,
    XR_VERSION_MAJOR(graphicsRequirements.minApiVersionSupported),
    XR_VERSION_MINOR(graphicsRequirements.minApiVersionSupported),
    0
));
```

But that's not all, we also need to tell Vulkan we intend to use multiview during device creation, which we do like so:

```cpp
VkPhysicalDeviceMultiviewFeatures multiviewFeatures{};
multiviewFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
multiviewFeatures.multiview = true;

VkDeviceCreateInfo createInfo{};
createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
createInfo.pNext = &multiviewFeatures;
createInfo.queueCreateInfoCount = 1;
createInfo.pQueueCreateInfos = &queueCreateInfo;
createInfo.enabledExtensionCount = extensionCount;
createInfo.ppEnabledExtensionNames = extensions;
```

Keep in mind that the `VkPhysicalDeviceMultiviewFeatures` structure has `multiviewGeometryShader` and `multiviewTessellationShader` members as well which I've omitted here because we won't be using them. If you intend to combine those kinds of shaders with a multiview rendering context you'll need to enable those flags as well. Moving on, the next step is to alter the render pass. Pipelines don't change between monoview and multiview rendering but render passes do. Here's our updated render pass creation info:

```cpp
uint32_t viewMask = 0b11;
uint32_t correlationMask = 0b11;

VkRenderPassMultiviewCreateInfo multiview{};
multiview.sType = VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
multiview.subpassCount = 1;
multiview.pViewMasks = &viewMask;
multiview.correlationMaskCount = 1;
multiview.pCorrelationMasks = &correlationMask;

VkRenderPassCreateInfo createInfo{};
createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
createInfo.pNext = &multiview;
createInfo.flags = 0;
createInfo.attachmentCount = 1;
createInfo.pAttachments = &attachment;
createInfo.subpassCount = 1;
createInfo.pSubpasses = &subpass;
```

We add a pointer to a secondary structure which in turn points to two bitmask-style integers. The first one, `viewMask`, tells Vulkan which views we intend to render into. We intend to render into the first and second views, so we set the two lowest bits. The second one, `correlationMask`, tells Vulkan which views are likely to experience a speed-up by being rendered together e.g. because the same objects are likely in frame in both. Our cameras are going to be very close together and facing in the same direction so again we set both bits. Correlation masking is an optimization hint, it isn't guaranteed the graphics driver will actually do anything with it and it can be omitted entirely if you like. It's also worth noting that there is a more modern way of providing this information during render pass creation. If you have the `VK_KHR_create_renderpass2` extension or Vulkan 1.2+ you can use `vkCreateRenderPass2`. This is a newer version of the whole render pass creation API that fixes a number of deficiencies in the original, including putting support for multiview directly into the subpass description (now called `VkSubpassDescription2`).

The next thing we need to alter is the initialization of our `SwapchainImage` class. The swapchain `VkImage` we get from OpenXR will be different now, because of the different `XrSwapchainCreateInfo::arraySize` parameter we set, so the `VkImageView` we create needs to be different too:

```cpp
VkImageViewCreateInfo imageViewCreateInfo{};
imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
imageViewCreateInfo.image = image.image;
imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
imageViewCreateInfo.format = swapchain->format;
imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
imageViewCreateInfo.subresourceRange.levelCount = 1;
imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
imageViewCreateInfo.subresourceRange.layerCount = eyeCount;
```

There are two changes here, we change `viewType` to `VK_IMAGE_VIEW_TYPE_2D_ARRAY` and `layerCount` to `eyeCount`. The first change tells Vulkan this is an array of 2D images while the second tells Vulkan that the array contains two such images. While it might look tempting it's important <strong>not</strong> to change `VkFramebufferCreateInfo::layers` below this code block, that should remain 1 because despite the name it's actually unrelated to multiview rendering. The final thing we need to change in the `SwapchainImage` class is the size of the uniform buffer. Previously we had set this to the size of three single-precision 4×4 matrices: one for the projection transform, one for the view transform and one for the model transform. We need to increase its size now to hold five matrices: two projections, two views and one model. This will give us room to store one projection matrix and one view matrix per-eye.

```cpp
static const size_t bufferSize = sizeof(float) * 4 * 4 * 5;
```

Now let's talk about shaders. Our fragment shader can remain unchanged but our vertex shader needs to change like so:

```glsl
#version 450
#extension GL_EXT_multiview : enable

layout(location = 0) out vec3 color;

layout(binding = 0) uniform Matrices {
    mat4 projection[2];
    mat4 view[2];
    mat4 model;
} matrices;

vec3 vertices[3] = vec3[](
    vec3(0, +1, 0),
    vec3(-0.866025, -0.5, 0),
    vec3(+0.866025, -0.5, 0)
);

void main()
{
    gl_Position = matrices.projection[gl_ViewIndex] * matrices.view[gl_ViewIndex] * matrices.model * vec4(vertices[gl_VertexIndex], 1);
    color = abs(vertices[gl_VertexIndex]);
}
```

We've added a directive at the top of the file that enables the `GL_EXT_multiview` extension. This is a GLSL extension that adds one new global constant for us to use: `gl_ViewIndex`. We update the definition of our uniform buffer to contain two copies of the projection and view matrices and then update our main function to index these matrix arrays using the new global constant `gl_ViewIndex`.

We're almost done now, all that's left is to change how we approach rendering and submitting our scene. First we replace the for loop that executed `renderEye` twice with a single call to `renderEyes`:

```cpp
bool ok = renderEyes(
    swapchain,
    swapchainImages,
    views,
    device,
    queue,
    renderPass,
    pipelineLayout,
    pipeline
);

if (!ok)
{
    return false;
}
```

The `renderEyes` function is almost the same as the old `renderEye` function but with one difference we'll go over now. The difference is it takes both `XrView`s as arguments rather than just one and then uses both to fill the uniform buffer with five matrices rather than three, like so:

```cpp
for (size_t i = 0; i < eyeCount; i++)
{
    float angleWidth = tan(views[i].fov.angleRight) - tan(views[i].fov.angleLeft);
    float angleHeight = tan(views[i].fov.angleDown) - tan(views[i].fov.angleUp);

    float projectionMatrix[4][4]{0};

    projectionMatrix[0][0] = 2.0f / angleWidth;
    projectionMatrix[2][0] = (tan(views[i].fov.angleRight) + tan(views[i].fov.angleLeft)) / angleWidth;
    projectionMatrix[1][1] = 2.0f / angleHeight;
    projectionMatrix[2][1] = (tan(views[i].fov.angleUp) + tan(views[i].fov.angleDown)) / angleHeight;
    projectionMatrix[2][2] = -farDistance / (farDistance - nearDistance);
    projectionMatrix[3][2] = -(farDistance * nearDistance) / (farDistance - nearDistance);
    projectionMatrix[2][3] = -1;

    memcpy(data, projectionMatrix, sizeof(float) * 4 * 4);
    data += 4 * 4;
}

for (size_t i = 0; i < eyeCount; i++)
{
    glm::mat4 viewMatrix = glm::inverse(
        glm::translate(glm::mat4(1.0f), glm::vec3(views[i].pose.position.x, views[i].pose.position.y, views[i].pose.position.z))
        * glm::mat4_cast(glm::quat(views[i].pose.orientation.w, views[i].pose.orientation.x, views[i].pose.orientation.y, views[i].pose.orientation.z))
    );

    memcpy(data, glm::value_ptr(viewMatrix), sizeof(float) * 4 * 4);
    data += 4 * 4;
}

float modelMatrix[4][4]{
    { 1, 0, 0, 0 },
    { 0, 1, 0, 0 },
    { 0, 0, 1, 0 },
    { objectPos.x, objectPos.y, objectPos.z, 1 }
};

memcpy(data, modelMatrix, sizeof(float) * 4 * 4);
```

The rest of the function, acquiring and releasing images, recording and submitting draw commands, etc. is all completely unchanged. The final thing we need to do is tell OpenXR where to find our images when we submit them. We do this by altering the `XrCompositionLayerProjectionView` structures that will be submitted to `xrEndFrame`. There are still two such structures, that hasn't changed, but now they're initialized like this:

```cpp
XrCompositionLayerProjectionView projectedViews[2]{};

for (size_t i = 0; i < eyeCount; i++)
{
    projectedViews[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
    projectedViews[i].pose = views[i].pose;
    projectedViews[i].fov = views[i].fov;
    projectedViews[i].subImage = {
        swapchain->swapchain,
        {
            { 0, 0 },
            { (int32_t)swapchain->width, (int32_t)swapchain->height }
        },
        (uint32_t)i
    };
}
```

They both now reference the same swapchain and instead the iterator `i` is used to fill in the last field of the `subImage` member, which is `XrSwapchainSubImage::imageArrayIndex`. This tells OpenXR that we want to use the same image for both eyes but use a different array layer within that image for each.

And that's basically it! There are a handful of changes I didn't mention here: changing the parameters of `render` to only take one swapchain, changing the cleanup code to only delete one swapchain and one set of images, etc. but those don't really need any additional explanation and you can always refer to the example which includes all the changes in their entirety. Overall this approach should result in a much simpler program that's more performant. The main highlight of what we've achieved here are that by only invoking `renderEye`/`renderEyes` once we've cut the number of draw calls in half, the number of render passes used in half and the number of command buffer submissions in half. All of these represent quite significant performance costs in modern graphics programming so it's nice to reduce them by such a substantial factor.

**Note:** If you're using SteamVR you might notice you get continuous per-frame Vulkan validation errors about image layouts when you run your program/the example. This is a [known bug](https://github.com/ValveSoftware/openvr/issues/1822) in SteamVR caused by it failing to adhere to the OpenXR standard. You can safely ignore the warnings, they don't seem to have any effect on the final visual result.
