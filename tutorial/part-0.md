This tutorial covers the basics of using the OpenXR API. OpenXR is an up-and-coming platform-agnostic API for accessing a variety of XR hardware, which intends to replace legacy APIs like OpenVR. This tutorial will cover the absolute basics of using the OpenXR API directly, from the ground up. If you just want to get started making something cool with XR you might be better off using the support already integrated into many major game engines.

I wrote this because when I set out to use OpenXR in [my own game](https://lambdamod.org/) and found very little information on how to do so. In the end I had to learn the API from the reference guide and the specification directly. Given that experience, I thought I'd try make things easier for the next person to attempt this.

There's a lot of ground to cover here so for simplicity's sake I'm going to assume the following:

- You are comfortable with C++.
- You are comfortable with Vulkan.
- You use Linux.

Most of this tutorial still applies for other languages, graphics APIs and operating systems, but you will need to some extra legwork to find the equivalents for certain things.

## Part 0: Getting Started

OpenXR uses a system with a "loader" and one or more "runtimes". A runtime is a software platform supporting some set of physical XR devices, while the loader is an application that selects which runtime to use when an application is launched. This system should be familiar to anyone who has worked with Vulkan or OpenCL in the past.

To get started you will first need to install an OpenXR loader for your platform:

On Debian Linux and derivatives (e.g. Ubuntu, Mint, Pop!):

```
sudo apt install libopenxr-dev libopenxr-loader1 openxr-layer-corevalidation
```

On Arch Linux and derivatives (e.g. Endeavour, Manjaro):

```
sudo pacman -S openxr
```

You will also need to have Vulkan installed.

After that you will need to install a runtime. Examples of runtimes include SteamVR and Monado XR. I'm not going to include instructions here because if you're following this tutorial you probably already have SteamVR installed and Monado XR is very difficult to install on my platform (Arch) at the time of writing.

Next you will need to tell OpenXR that your runtime exists and is available for use. To do so create the following directory:

```
~/.config/openxr/1/
```

And then create a file inside called `active_runtime.json`. Inside of the file you can specify a name and the path to your runtime. SteamVR also has a button in its settings UI to do this automatically. for SteamVR:

```
{
	"file_format_version": "1.0.0",
	"runtime": 
	{
		"VALVE_runtime_is_steamvr": true,
		"library_path": "/home/YOUR_USERNAME/.local/share/Steam/steamapps/common/SteamVR/bin/linux64/vrclient.so",
		"name": "SteamVR"
	}
}
```

And for Monado XR:

```
{
	"file_format_version": "1.0.0",
	"runtime": 
	{
		"library_path": "/usr/lib/libopenxr_monado.so",
		"name": "Monado"
	}
}
```

Now we can start programming. We'll start with this basic file:

```
#include <vulkan/vulkan.h>
#define XR_USE_GRAPHICS_API_VULKAN
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#include <iostream>

using namespace std;

int main(int, char**)
{
    cout << "Hello world!" << endl;

    return 0;
}
```

Defining `XR_USE_GRAPHICS_API_VULKAN` enables parts of the OpenXR API specific to Vulkan. If you are using a different graphics API you'll want to swap that out. Call this file `openxr_example.cpp` and then compile it with:

```
g++ -o openxr_example -lopenxr_loader -lvulkan openxr_example.cpp
```

If you run `./openxr_example` you should see it print out "Hello world!" and then exit. That's the basic setup of OpenXR done, next time we can see about initializing it.
