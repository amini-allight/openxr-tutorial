# OpenXR Tutorial

This repository contains the source code for my OpenXR tutorial.

## Changelog

- **27th June 2024:** Fixed addendum 3 not handling all possible ways a display server might request recreation of the Vulkan swapchain, switched to mailbox presentation mode in the same tutorial to prevent a crash on X11.
- **25th April 2024:** Fixed the tutorial requesting `XR_CURRENT_API_VERSION`, currently this value is 1.1 on many systems but SteamVR does not yet support that API version. Fixed the program not attempting to exit when an error occurred in the `renderEye` or `renderEyes` functions.
- **22nd March 2024:** Fixed a minor mistake in parts 4 through 9 where the render pass was initialized with a fixed image format that might not match the actual format provided by the swapchain.
- **11th October 2023:** Fixed a mistake in parts 4 through 9 where the Vulkan API version was set incorrectly. The old version assumed OpenXR and Vulkan use the same format for their packed version types, but they do not. Replaced this with the correct version.

## License

Created by Amini Allight. The contents of this repository are licensed under Creative Commons Zero (CC0 1.0), placing them in the public domain.

The one exception to this is the file `vk_mem_alloc.h` taken from AMD's [Vulkan Memory Allocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator) project, the license details of which can be found in the header of that same file.

This tutorial is not affiliated with or endorsed by the Khronos Group.
