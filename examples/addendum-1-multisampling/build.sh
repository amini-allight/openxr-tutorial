#!/bin/sh
glslangValidator --quiet -V -S vert -o vertex.spv vertex.glsl
glslangValidator --quiet -V -S frag -o fragment.spv fragment.glsl
g++ -o openxr_example_runtime_resolution -lopenxr_loader -lvulkan openxr_example_runtime_resolution.cpp
g++ -o openxr_example_application_resolution -lopenxr_loader -lvulkan openxr_example_application_resolution.cpp
