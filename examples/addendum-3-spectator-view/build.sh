#!/bin/sh
glslangValidator --quiet -V -S vert -o vertex.spv vertex.glsl
glslangValidator --quiet -V -S frag -o fragment.spv fragment.glsl
g++ -g -o openxr_example -lopenxr_loader -lSDL2 -lvulkan openxr_example.cpp
