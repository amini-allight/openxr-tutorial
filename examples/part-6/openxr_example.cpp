#include <vulkan/vulkan.h>
#define XR_USE_GRAPHICS_API_VULKAN
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#include <iostream>
#include <cstring>
#include <tuple>
#include <set>
#include <vector>
#include <fstream>
#include <streambuf>

using namespace std;

static const char* const applicationName = "OpenXR Example";
static const unsigned int majorVersion = 0;
static const unsigned int minorVersion = 1;
static const unsigned int patchVersion = 0;
static const char* const layerNames[] = { "XR_APILAYER_LUNARG_core_validation" };
static const char* const extensionNames[] = {
    "XR_KHR_vulkan_enable",
    "XR_KHR_vulkan_enable2",
    "XR_EXT_debug_utils"
};
static const char* const vulkanLayerNames[] = { "VK_LAYER_KHRONOS_validation" };
static const char* const vulkanExtensionNames[] = { "VK_EXT_debug_utils" };

static const size_t bufferSize = sizeof(float) * 4 * 4 * 3;

static const size_t eyeCount = 2;

struct Swapchain
{
    Swapchain(XrSwapchain swapchain, VkFormat format, uint32_t width, uint32_t height)
        : swapchain(swapchain)
        , format(format)
        , width(width)
        , height(height)
    {

    }

    ~Swapchain()
    {
        xrDestroySwapchain(swapchain);
    }

    XrSwapchain swapchain;
    VkFormat format;
    uint32_t width;
    uint32_t height;
};

PFN_xrVoidFunction getXRFunction(XrInstance instance, const char* name)
{
    PFN_xrVoidFunction func;

    XrResult result = xrGetInstanceProcAddr(instance, name, &func);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to load OpenXR extension function '" << name << "': " << result << endl;
        return nullptr;
    }

    return func;
}

PFN_vkVoidFunction getVKFunction(VkInstance instance, const char* name)
{
    PFN_vkVoidFunction func = vkGetInstanceProcAddr(instance, name);

    if (!func)
    {
        cerr << "Failed to load VUlkan extension function '" << name << "'." << endl;
        return nullptr;
    }

    return func;
}

XrBool32 handleXRError(
    XrDebugUtilsMessageSeverityFlagsEXT severity,
    XrDebugUtilsMessageTypeFlagsEXT type,
    const XrDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    cerr << "OpenXR ";

    switch (type)
    {
    case XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        cerr << "general ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        cerr << "validation ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        cerr << "performance ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT :
        cerr << "conformance ";
        break;
    }

    switch (severity)
    {
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        cerr << "(verbose): ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        cerr << "(info): ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        cerr << "(warning): ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << "(error): ";
        break;
    }

    cerr << callbackData->message << endl;

    return XR_FALSE;
}

VkBool32 handleVKError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    cerr << "Vulkan ";

    switch (type)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        cerr << "general ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        cerr << "validation ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        cerr << "performance ";
        break;
    }

    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        cerr << "(verbose): ";
        break;
    default :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        cerr << "(info): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        cerr << "(warning): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << "(error): ";
        break;
    }

    cerr << callbackData->pMessage << endl;

    return VK_FALSE;
}

XrInstance createInstance()
{
    XrInstance instance;

    XrInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.type = XR_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.createFlags = 0;
    strcpy(instanceCreateInfo.applicationInfo.applicationName, applicationName);
    instanceCreateInfo.applicationInfo.applicationVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    strcpy(instanceCreateInfo.applicationInfo.engineName, applicationName);
    instanceCreateInfo.applicationInfo.engineVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    instanceCreateInfo.applicationInfo.apiVersion = XR_MAKE_VERSION(1, 0, 34);
    instanceCreateInfo.enabledApiLayerCount = 1;
    instanceCreateInfo.enabledApiLayerNames = layerNames;
    instanceCreateInfo.enabledExtensionCount = sizeof(extensionNames) / sizeof(const char*);
    instanceCreateInfo.enabledExtensionNames = extensionNames;

    XrResult result = xrCreateInstance(&instanceCreateInfo, &instance);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR instance: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return instance;
}

void destroyInstance(XrInstance instance)
{
    xrDestroyInstance(instance);
}

XrDebugUtilsMessengerEXT createDebugMessenger(XrInstance instance)
{
    XrDebugUtilsMessengerEXT debugMessenger;

    XrDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
    debugMessengerCreateInfo.type = XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugMessengerCreateInfo.messageSeverities = XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageTypes = XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.userCallback = handleXRError;
    debugMessengerCreateInfo.userData = nullptr;

    auto xrCreateDebugUtilsMessengerEXT = (PFN_xrCreateDebugUtilsMessengerEXT)getXRFunction(instance, "xrCreateDebugUtilsMessengerEXT");

    XrResult result = xrCreateDebugUtilsMessengerEXT(instance, &debugMessengerCreateInfo, &debugMessenger);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR debug messenger: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return debugMessenger;
}

void destroyDebugMessenger(XrInstance instance, XrDebugUtilsMessengerEXT debugMessenger)
{
    auto xrDestroyDebugUtilsMessengerEXT = (PFN_xrDestroyDebugUtilsMessengerEXT)getXRFunction(instance, "xrDestroyDebugUtilsMessengerEXT");

    xrDestroyDebugUtilsMessengerEXT(debugMessenger);
}

XrSystemId getSystem(XrInstance instance)
{
    XrSystemId systemID;

    XrSystemGetInfo systemGetInfo{};
    systemGetInfo.type = XR_TYPE_SYSTEM_GET_INFO;
    systemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

    XrResult result = xrGetSystem(instance, &systemGetInfo, &systemID);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get system: " << result << endl;
        return XR_NULL_SYSTEM_ID;
    }

    return systemID;
}

tuple<XrGraphicsRequirementsVulkanKHR, set<string>> getVulkanInstanceRequirements(XrInstance instance, XrSystemId system)
{
    auto xrGetVulkanGraphicsRequirementsKHR = (PFN_xrGetVulkanGraphicsRequirementsKHR)getXRFunction(instance, "xrGetVulkanGraphicsRequirementsKHR");
    auto xrGetVulkanInstanceExtensionsKHR = (PFN_xrGetVulkanInstanceExtensionsKHR)getXRFunction(instance, "xrGetVulkanInstanceExtensionsKHR");

    XrGraphicsRequirementsVulkanKHR graphicsRequirements{};
    graphicsRequirements.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR;

    XrResult result = xrGetVulkanGraphicsRequirementsKHR(instance, system, &graphicsRequirements);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan graphics requirements: " << result << endl;
        return { graphicsRequirements, {} };
    }

    uint32_t instanceExtensionsSize;

    result = xrGetVulkanInstanceExtensionsKHR(instance, system, 0, &instanceExtensionsSize, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan instance extensions: " << result << endl;
        return { graphicsRequirements, {} };
    }

    char* instanceExtensionsData = new char[instanceExtensionsSize];

    result = xrGetVulkanInstanceExtensionsKHR(instance, system, instanceExtensionsSize, &instanceExtensionsSize, instanceExtensionsData);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan instance extensions: " << result << endl;
        return { graphicsRequirements, {} };
    }

    set<string> instanceExtensions;

    uint32_t last = 0;
    for (uint32_t i = 0; i <= instanceExtensionsSize; i++)
    {
        if (i == instanceExtensionsSize || instanceExtensionsData[i] == ' ')
        {
            instanceExtensions.insert(string(instanceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] instanceExtensionsData;

    return { graphicsRequirements, instanceExtensions };
}

tuple<VkPhysicalDevice, set<string>> getVulkanDeviceRequirements(XrInstance instance, XrSystemId system, VkInstance vulkanInstance)
{
    auto xrGetVulkanGraphicsDeviceKHR = (PFN_xrGetVulkanGraphicsDeviceKHR)getXRFunction(instance, "xrGetVulkanGraphicsDeviceKHR");
    auto xrGetVulkanDeviceExtensionsKHR = (PFN_xrGetVulkanDeviceExtensionsKHR)getXRFunction(instance, "xrGetVulkanDeviceExtensionsKHR");

    VkPhysicalDevice physicalDevice;

    XrResult result = xrGetVulkanGraphicsDeviceKHR(instance, system, vulkanInstance, &physicalDevice);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan graphics device: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    uint32_t deviceExtensionsSize;

    result = xrGetVulkanDeviceExtensionsKHR(instance, system, 0, &deviceExtensionsSize, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan device extensions: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    char* deviceExtensionsData = new char[deviceExtensionsSize];

    result = xrGetVulkanDeviceExtensionsKHR(instance, system, deviceExtensionsSize, &deviceExtensionsSize, deviceExtensionsData);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan device extensions: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    set<string> deviceExtensions;

    uint32_t last = 0;
    for (uint32_t i = 0; i <= deviceExtensionsSize; i++)
    {
        if (i == deviceExtensionsSize || deviceExtensionsData[i] == ' ')
        {
            deviceExtensions.insert(string(deviceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] deviceExtensionsData;

    return { physicalDevice, deviceExtensions };
}

XrSession createSession(
    XrInstance instance,
    XrSystemId systemID,
    VkInstance vulkanInstance,
    VkPhysicalDevice physDevice,
    VkDevice device,
    uint32_t queueFamilyIndex
)
{
    XrSession session;

    XrGraphicsBindingVulkanKHR graphicsBinding{};
    graphicsBinding.type = XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR;
    graphicsBinding.instance = vulkanInstance;
    graphicsBinding.physicalDevice = physDevice;
    graphicsBinding.device = device;
    graphicsBinding.queueFamilyIndex = queueFamilyIndex;
    graphicsBinding.queueIndex = 0;

    XrSessionCreateInfo sessionCreateInfo{};
    sessionCreateInfo.type = XR_TYPE_SESSION_CREATE_INFO;
    sessionCreateInfo.next = &graphicsBinding;
    sessionCreateInfo.createFlags = 0;
    sessionCreateInfo.systemId = systemID;

    XrResult result = xrCreateSession(instance, &sessionCreateInfo, &session);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR session: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return session;
}

void destroySession(XrSession session)
{
    xrDestroySession(session);
}

tuple<Swapchain*, Swapchain*> createSwapchains(XrInstance instance, XrSystemId system, XrSession session)
{
    uint32_t configViewsCount = eyeCount;
    vector<XrViewConfigurationView> configViews(
        configViewsCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    XrResult result = xrEnumerateViewConfigurationViews(
        instance,
        system,
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewsCount,
        &configViewsCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate view configuration views: " << result << endl;
        return { nullptr, nullptr };
    }

    uint32_t formatCount = 0;

    result = xrEnumerateSwapchainFormats(session, 0, &formatCount, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain formats: " << result << endl;
        return { nullptr, nullptr };
    }

    vector<int64_t> formats(formatCount);

    result = xrEnumerateSwapchainFormats(session, formatCount, &formatCount, formats.data());

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain formats: " << result << endl;
        return { nullptr, nullptr };
    }

    int64_t chosenFormat = formats.front();

    for (int64_t format : formats)
    {
        if (format == VK_FORMAT_R8G8B8A8_SRGB)
        {
            chosenFormat = format;
            break;
        }
    }

    XrSwapchain swapchains[eyeCount];

    for (uint32_t i = 0; i < eyeCount; i++)
    {
        XrSwapchainCreateInfo swapchainCreateInfo{};
        swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
        swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
        swapchainCreateInfo.format = chosenFormat;
        swapchainCreateInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
        swapchainCreateInfo.width = configViews[i].recommendedImageRectWidth;
        swapchainCreateInfo.height = configViews[i].recommendedImageRectHeight;
        swapchainCreateInfo.faceCount = 1;
        swapchainCreateInfo.arraySize = 1;
        swapchainCreateInfo.mipCount = 1;

        result = xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]);

        if (result != XR_SUCCESS)
        {
            cerr << "Failed to create swapchain: " << result << endl;
            return { nullptr, nullptr };
        }
    }

    return {
        new Swapchain(
            swapchains[0],
            (VkFormat)chosenFormat,
            configViews[0].recommendedImageRectWidth,
            configViews[0].recommendedImageRectHeight
        ),
        new Swapchain(
            swapchains[1],
            (VkFormat)chosenFormat,
            configViews[1].recommendedImageRectWidth,
            configViews[1].recommendedImageRectHeight
        )
    };
}

vector<XrSwapchainImageVulkanKHR> getSwapchainImages(XrSwapchain swapchain)
{
    uint32_t imageCount;

    XrResult result = xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain images: " << result << endl;
        return {};
    }

    vector<XrSwapchainImageVulkanKHR> images(
        imageCount,
        { .type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR }
    );

    result = xrEnumerateSwapchainImages(swapchain, imageCount, &imageCount, (XrSwapchainImageBaseHeader*)images.data());

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain images: " << result << endl;
        return {};
    }

    return images;
}

VkInstance createVulkanInstance(XrGraphicsRequirementsVulkanKHR graphicsRequirements, set<string> instanceExtensions)
{
    VkInstance instance;

    size_t extensionCount = 1 + instanceExtensions.size();
    const char** extensionNames = new const char*[extensionCount];

    size_t i = 0;
    extensionNames[i] = vulkanExtensionNames[0];
    i++;

    for (const string& instanceExtension : instanceExtensions)
    {
        extensionNames[i] = instanceExtension.c_str();
        i++;
    }

    VkApplicationInfo applicationInfo{};
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pApplicationName = applicationName;
    applicationInfo.applicationVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    applicationInfo.pEngineName = applicationName;
    applicationInfo.engineVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    applicationInfo.apiVersion = VK_MAKE_API_VERSION(
        0,
        XR_VERSION_MAJOR(graphicsRequirements.minApiVersionSupported),
        XR_VERSION_MINOR(graphicsRequirements.minApiVersionSupported),
        0
    );

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &applicationInfo;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensionNames;
    createInfo.enabledLayerCount = 1;
    createInfo.ppEnabledLayerNames = vulkanLayerNames;

    VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

    delete[] extensionNames;

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan instance: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return instance;
}

void destroyVulkanInstance(VkInstance instance)
{
    vkDestroyInstance(instance, nullptr);
}

VkDebugUtilsMessengerEXT createVulkanDebugMessenger(VkInstance instance)
{
    VkDebugUtilsMessengerEXT debugMessenger;

    VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
    debugMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.pfnUserCallback = handleVKError;

    auto vkCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT)getVKFunction(instance, "vkCreateDebugUtilsMessengerEXT");

    VkResult result = vkCreateDebugUtilsMessengerEXT(instance, &debugMessengerCreateInfo, nullptr, &debugMessenger);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan debug messenger: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return debugMessenger;
}

void destroyVulkanDebugMessenger(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger)
{
    auto vkDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)getVKFunction(instance, "vkDestroyDebugUtilsMessengerEXT");

    vkDestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
}

int32_t getDeviceQueueFamily(VkPhysicalDevice physicalDevice)
{
    int32_t graphicsQueueFamilyIndex = -1;

    uint32_t queueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

    vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

    for (int32_t i = 0; i < queueFamilyCount; i++)
    {
        if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            graphicsQueueFamilyIndex = i;
            break;
        }
    }

    if (graphicsQueueFamilyIndex == -1)
    {
        cerr << "No graphics queue found." << endl;
        return graphicsQueueFamilyIndex;
    }

    return graphicsQueueFamilyIndex;
}

tuple<VkDevice, VkQueue> createDevice(
    VkPhysicalDevice physicalDevice,
    int32_t graphicsQueueFamilyIndex,
    set<string> deviceExtensions
)
{
    VkDevice device;

    size_t extensionCount = deviceExtensions.size();
    const char** extensions = new const char*[extensionCount];

    size_t i = 0;
    for (const string& deviceExtension : deviceExtensions)
    {
        extensions[i] = deviceExtension.c_str();
        i++;
    }

    float priority = 1;

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = graphicsQueueFamilyIndex;
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &priority;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensions;

    VkResult result = vkCreateDevice(physicalDevice, &createInfo, nullptr, &device);

    delete[] extensions;

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan device: " << result << endl;
        return { VK_NULL_HANDLE, VK_NULL_HANDLE };
    }

    VkQueue queue;
    vkGetDeviceQueue(device, graphicsQueueFamilyIndex, 0, &queue);

    return { device, queue };
}

void destroyDevice(VkDevice device)
{
    vkDestroyDevice(device, nullptr);
}

VkRenderPass createRenderPass(VkDevice device, VkFormat format)
{
    VkRenderPass renderPass;

    VkAttachmentDescription attachment{};
    attachment.format = format;
    attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference attachmentRef{};
    attachmentRef.attachment = 0;
    attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &attachmentRef;

    VkRenderPassCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    createInfo.flags = 0;
    createInfo.attachmentCount = 1;
    createInfo.pAttachments = &attachment;
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpass;

    VkResult result = vkCreateRenderPass(device, &createInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan render pass: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return renderPass;
}

void destroyRenderPass(VkDevice device, VkRenderPass renderPass)
{
    vkDestroyRenderPass(device, renderPass, nullptr);
}

VkCommandPool createCommandPool(VkDevice device, int32_t graphicsQueueFamilyIndex)
{
    VkCommandPool commandPool;

    VkCommandPoolCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    createInfo.queueFamilyIndex = graphicsQueueFamilyIndex;

    VkResult result = vkCreateCommandPool(device, &createInfo, nullptr, &commandPool);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan command pool: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return commandPool;
}

void destroyCommandPool(VkDevice device, VkCommandPool commandPool)
{
    vkDestroyCommandPool(device, commandPool, nullptr);
}

VkDescriptorPool createDescriptorPool(VkDevice device)
{
    VkDescriptorPool descriptorPool;

    VkDescriptorPoolSize poolSize{};
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSize.descriptorCount = 32;

    VkDescriptorPoolCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    createInfo.maxSets = 32;
    createInfo.poolSizeCount = 1;
    createInfo.pPoolSizes = &poolSize;

    VkResult result = vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan descriptor pool: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return descriptorPool;
}

void destroyDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool)
{
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
}

VkDescriptorSetLayout createDescriptorSetLayout(VkDevice device)
{
    VkDescriptorSetLayout descriptorSetLayout;

    VkDescriptorSetLayoutBinding binding{};
    binding.binding = 0;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    binding.descriptorCount = 1;
    binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = 1;
    createInfo.pBindings = &binding;

    VkResult result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan descriptor set layout: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return descriptorSetLayout;
}

void destroyDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout descriptorSetLayout)
{
    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
}

VkShaderModule createShader(VkDevice device, string path)
{
    VkShaderModule shader;

    ifstream file(path);
    string source = string(istreambuf_iterator<char>(file), istreambuf_iterator<char>());

    VkShaderModuleCreateInfo shaderCreateInfo{};
    shaderCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderCreateInfo.codeSize = source.size();
    shaderCreateInfo.pCode = (const uint32_t*)source.data();

    VkResult result = vkCreateShaderModule(device, &shaderCreateInfo, nullptr, &shader);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan shader: " << result << endl;
    }

    return shader;
}

void destroyShader(VkDevice device, VkShaderModule shader)
{
    vkDestroyShaderModule(device, shader, nullptr);
}

tuple<VkPipelineLayout, VkPipeline> createPipeline(
    VkDevice device,
    VkRenderPass renderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader
)
{
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;

    VkPipelineLayoutCreateInfo layoutCreateInfo{};
    layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    layoutCreateInfo.setLayoutCount = 1;
    layoutCreateInfo.pSetLayouts = &descriptorSetLayout;

    VkResult result = vkCreatePipelineLayout(device, &layoutCreateInfo, nullptr, &pipelineLayout);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan pipeline layout: " << result << endl;
        return { VK_NULL_HANDLE, VK_NULL_HANDLE };
    }

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = 0;
    vertexInputStage.pVertexBindingDescriptions = nullptr;
    vertexInputStage.vertexAttributeDescriptionCount = 0;
    vertexInputStage.pVertexAttributeDescriptions = nullptr;

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage{};
    inputAssemblyStage.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyStage.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssemblyStage.primitiveRestartEnable = false;

    VkPipelineShaderStageCreateInfo vertexShaderStage{};
    vertexShaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertexShaderStage.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertexShaderStage.module = vertexShader;
    vertexShaderStage.pName = "main";

    VkViewport viewport = {
        0, 0,
        1024, 1024,
        0, 1
    };

    VkRect2D scissor = {
        { 0, 0 },
        { 1024, 1024 }
    };

    VkPipelineViewportStateCreateInfo viewportStage{};
    viewportStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportStage.viewportCount = 1;
    viewportStage.pViewports = &viewport;
    viewportStage.scissorCount = 1;
    viewportStage.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizationStage{};
    rasterizationStage.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationStage.depthClampEnable = false;
    rasterizationStage.rasterizerDiscardEnable = false;
    rasterizationStage.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationStage.lineWidth = 1;
    rasterizationStage.cullMode = VK_CULL_MODE_NONE;
    rasterizationStage.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationStage.depthBiasEnable = false;
    rasterizationStage.depthBiasConstantFactor = 0;
    rasterizationStage.depthBiasClamp = 0;
    rasterizationStage.depthBiasSlopeFactor = 0;

    VkPipelineMultisampleStateCreateInfo multisampleStage{};
    multisampleStage.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampleStage.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampleStage.sampleShadingEnable = false;
    multisampleStage.minSampleShading = 0.25;

    VkPipelineDepthStencilStateCreateInfo depthStencilStage{};
    depthStencilStage.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilStage.depthTestEnable = true;
    depthStencilStage.depthWriteEnable = true;
    depthStencilStage.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilStage.depthBoundsTestEnable = false;
    depthStencilStage.minDepthBounds = 0;
    depthStencilStage.maxDepthBounds = 1;
    depthStencilStage.stencilTestEnable = false;

    VkPipelineShaderStageCreateInfo fragmentShaderStage{};
    fragmentShaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragmentShaderStage.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragmentShaderStage.module = fragmentShader;
    fragmentShaderStage.pName = "main";

    VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = true;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    VkPipelineColorBlendStateCreateInfo colorBlendStage{};
    colorBlendStage.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendStage.logicOpEnable = false;
    colorBlendStage.logicOp = VK_LOGIC_OP_COPY;
    colorBlendStage.attachmentCount = 1;
    colorBlendStage.pAttachments = &colorBlendAttachment;
    colorBlendStage.blendConstants[0] = 0;
    colorBlendStage.blendConstants[1] = 0;
    colorBlendStage.blendConstants[2] = 0;
    colorBlendStage.blendConstants[3] = 0;

    VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = 2;
    dynamicState.pDynamicStates = dynamicStates;

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    createInfo.stageCount = 2;
    createInfo.pStages = shaderStages;
    createInfo.pVertexInputState = &vertexInputStage;
    createInfo.pInputAssemblyState = &inputAssemblyStage;
    createInfo.pTessellationState = nullptr;
    createInfo.pViewportState = &viewportStage;
    createInfo.pRasterizationState = &rasterizationStage;
    createInfo.pMultisampleState = &multisampleStage;
    createInfo.pDepthStencilState = &depthStencilStage;
    createInfo.pColorBlendState = &colorBlendStage;
    createInfo.pDynamicState = &dynamicState;
    createInfo.layout = pipelineLayout;
    createInfo.renderPass = renderPass;
    createInfo.subpass = 0;
    createInfo.basePipelineHandle = VK_NULL_HANDLE;
    createInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, nullptr, 1, &createInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan pipeline: " << result << endl;
        return { VK_NULL_HANDLE, VK_NULL_HANDLE };
    }

    return { pipelineLayout, pipeline };
}

void destroyPipeline(VkDevice device, VkPipelineLayout pipelineLayout, VkPipeline pipeline)
{
    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
}

int main(int, char**)
{
    XrInstance instance = createInstance();
    XrDebugUtilsMessengerEXT debugMessenger = createDebugMessenger(instance);
    XrSystemId system = getSystem(instance);

    XrGraphicsRequirementsVulkanKHR graphicsRequirements;
    set<string> instanceExtensions;
    tie(graphicsRequirements, instanceExtensions) = getVulkanInstanceRequirements(instance, system);
    VkInstance vulkanInstance = createVulkanInstance(graphicsRequirements, instanceExtensions);
    VkDebugUtilsMessengerEXT vulkanDebugMessenger = createVulkanDebugMessenger(vulkanInstance);

    VkPhysicalDevice physicalDevice;
    set<string> deviceExtensions;
    tie(physicalDevice, deviceExtensions) = getVulkanDeviceRequirements(instance, system, vulkanInstance);
    int32_t graphicsQueueFamilyIndex = getDeviceQueueFamily(physicalDevice);
    VkDevice device;
    VkQueue queue;
    tie(device, queue) = createDevice(physicalDevice, graphicsQueueFamilyIndex, deviceExtensions);

    XrSession session = createSession(instance, system, vulkanInstance, physicalDevice, device, graphicsQueueFamilyIndex);

    Swapchain* swapchains[eyeCount];
    tie(swapchains[0], swapchains[1]) = createSwapchains(instance, system, session);

    VkRenderPass renderPass = createRenderPass(device, swapchains[0]->format);
    VkCommandPool commandPool = createCommandPool(device, graphicsQueueFamilyIndex);
    VkDescriptorPool descriptorPool = createDescriptorPool(device);
    VkDescriptorSetLayout descriptorSetLayout = createDescriptorSetLayout(device);
    VkShaderModule vertexShader = createShader(device, "vertex.spv");
    VkShaderModule fragmentShader = createShader(device, "fragment.spv");
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    tie(pipelineLayout, pipeline) = createPipeline(device, renderPass, descriptorSetLayout, vertexShader, fragmentShader);

    vector<XrSwapchainImageVulkanKHR> swapchainImages[eyeCount];

    for (size_t i = 0; i < eyeCount; i++)
    {
        swapchainImages[i] = getSwapchainImages(swapchains[i]->swapchain);
    }

    destroyPipeline(device, pipelineLayout, pipeline);
    destroyShader(device, fragmentShader);
    destroyShader(device, vertexShader);
    destroyDescriptorSetLayout(device, descriptorSetLayout);
    destroyDescriptorPool(device, descriptorPool);
    destroyCommandPool(device, commandPool);
    destroyRenderPass(device, renderPass);

    for (size_t i = 0; i < eyeCount; i++)
    {
        delete swapchains[i];
    }

    destroySession(session);

    destroyDevice(device);

    destroyVulkanDebugMessenger(vulkanInstance, vulkanDebugMessenger);
    destroyVulkanInstance(vulkanInstance);

    destroyDebugMessenger(instance, debugMessenger);
    destroyInstance(instance);

    return 0;
}
