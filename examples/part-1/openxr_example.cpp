#include <vulkan/vulkan.h>
#define XR_USE_GRAPHICS_API_VULKAN
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#include <iostream>
#include <cstring>

using namespace std;

XrInstance createInstance()
{
    XrInstance instance;

    static const char* const applicationName = "OpenXR Example";
    static const unsigned int majorVersion = 0;
    static const unsigned int minorVersion = 1;
    static const unsigned int patchVersion = 0;
    static const char* const extensionNames[] = {
        "XR_KHR_vulkan_enable",
        "XR_KHR_vulkan_enable2"
    };

    XrInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.type = XR_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.createFlags = 0;
    strcpy(instanceCreateInfo.applicationInfo.applicationName, applicationName);
    instanceCreateInfo.applicationInfo.applicationVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    strcpy(instanceCreateInfo.applicationInfo.engineName, applicationName);
    instanceCreateInfo.applicationInfo.engineVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    instanceCreateInfo.applicationInfo.apiVersion = XR_MAKE_VERSION(1, 0, 34);
    instanceCreateInfo.enabledApiLayerCount = 0;
    instanceCreateInfo.enabledApiLayerNames = nullptr;
    instanceCreateInfo.enabledExtensionCount = sizeof(extensionNames) / sizeof(const char*);
    instanceCreateInfo.enabledExtensionNames = extensionNames;

    XrResult result = xrCreateInstance(&instanceCreateInfo, &instance);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR instance: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return instance;
}

void destroyInstance(XrInstance instance)
{
    xrDestroyInstance(instance);
}

int main(int, char**)
{
    XrInstance instance = createInstance();

    destroyInstance(instance);

    return 0;
}
