#!/bin/sh
glslangValidator --quiet -V -S vert -o vertex.spv vertex.glsl
glslangValidator --quiet -V -S frag -o fragment.spv fragment.glsl
g++ -o openxr_example -lopenxr_loader -lvulkan openxr_example.cpp
